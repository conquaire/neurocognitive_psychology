*******************************************************Learn effects*******************************************************
*******************************************************Read Learn Block Means.
*einlesen.
GET DATA  /TYPE=TXT
  /FILE="E:\Experimente\NCT\NCT_Results\NCT_ExpectationDiscrepancy\ExpectDiscrep_V8\results\SPSStabs\learn_block.txt"
  /ENCODING='Locale'
  /DELCASE=LINE
  /DELIMITERS="\t"
  /ARRANGEMENT=DELIMITED
  /FIRSTCASE=2
  /IMPORTCASE=ALL
  /VARIABLES=
  Subject F5.2
  Time1 F5.2
  Time2 F4.2
  Time3 F4.2
  Time4 F4.2
  Time5 F4.2
  Time6 F4.2
  NrErr1 F4.2
  NrErr2 F4.2
  NrErr3 F4.2
  NrErr4 F4.2
  NrErr5 F4.2
  NrErr6 F4.2
  ErrDist1 F6.2
  ErrDist2 F6.2
  ErrDist3 F6.2
  ErrDist4 F6.2
  ErrDist5 F6.2
  ErrDist6 F6.2
  NrFix1 F5.2
  NrFix2 F5.2
  NrFix3 F5.2
  NrFix4 F5.2
  NrFix5 F5.2
  NrFix6 F5.2
  FixDur1 F6.2
  FixDur2 F6.2
  FixDur3 F6.2
  FixDur4 F6.2
  FixDur5 F6.2
  FixDur6 F6.2
  Cpath1 F6.2
  Cpath2 F6.2
  Cpath3 F6.2
  Cpath4 F6.2
  Cpath5 F6.2
  Cpath6 F6.2
  Epath1 F6.2
  Epath2 F6.2
  Epath3 F6.2
  Epath4 F6.2
  Epath5 F6.2
  Epath6 F6.2
  ECdist1 F6.2
  ECdist2 F6.2
  ECdist3 F6.2
  ECdist4 F6.2
  ECdist5 F6.2
  ECdist6 F6.2.
CACHE.
EXECUTE.
DATASET NAME learn WINDOW=FRONT.

*******************BlockEffects*******************
*Test for normal distribution.
NPAR TESTS
  /K-S(NORMAL)=Time1 Time2 Time3 Time4 Time5 Time6
    NrErr1 NrErr2 NrErr3 NrErr4 NrErr5 NrErr6
    ErrDist1 ErrDist2 ErrDist3 ErrDist4 ErrDist5 ErrDist6
    NrFix1 NrFix2 NrFix3 NrFix4 NrFix5 NrFix6
    FixDur1 FixDur2 FixDur3 FixDur4 FixDur5 FixDur6
    Cpath1 Cpath2 Cpath3 Cpath4 Cpath5 Cpath6
    Epath1 Epath2 Epath3 Epath4 Epath5 Epath6
    ECdist1 ECdist2 ECdist3 ECdist4 ECdist5 ECdist6
  /STATISTICS DESCRIPTIVES QUARTILES
  /MISSING ANALYSIS
  /METHOD=EXACT TIMER(1).
*NotNormal(according to exact ps):
NrErr125,FixDur234.

*Time.
GLM Time1 Time2 Time3 Time4 Time5 Time6
  /WSFACTOR=block 6 Polynomial 
  /METHOD=SSTYPE(3)
  /PLOT=PROFILE(block)
  /EMMEANS=TABLES(block) COMPARE ADJ(LSD)
  /PRINT=DESCRIPTIVE ETASQ OPOWER PARAMETER 
  /CRITERIA=ALPHA(.05)
  /WSDESIGN=block.
Block*:F(5,195)=52.97,MSE=23.93,p=.000,eta=.58,power=1.00,epsilon=.52
linearTrend:F(1,39)=86.27,MSE=47.30,p=.000,eta=.69,power=1.00.
1:2-6:p<.000,2:3-6:p<.01,3:4-6:p<.05,4:5-6:n.s.,5:6:n.s.p=.220.

*NrErr.
GLM NrErr1 NrErr2 NrErr3 NrErr4 NrErr5 NrErr6
  /WSFACTOR=block 6 Polynomial 
  /METHOD=SSTYPE(3)
  /PLOT=PROFILE(block)
  /EMMEANS=TABLES(block) COMPARE ADJ(LSD)
  /PRINT=DESCRIPTIVE ETASQ OPOWER PARAMETER 
  /CRITERIA=ALPHA(.05)
  /WSDESIGN=block.
Block*:F(5,195)=11.97,MSE=1.66,p=.000,eta=.24, power=1.00, epsiolon=.58
linearTrend:F(1,39)=25.06,MSE=7.64,p=.000,eta=.39,power=1.00
*1:2-6 p<.05,2:3-6:p<.05,3:6:p<.05,4:5-6:p<.05,5:6:n.s.p=.430.
NPAR TESTS
  /FRIEDMAN=NrErr1 NrErr2 NrErr3 NrErr4 NrErr5 NrErr6
  /STATISTICS DESCRIPTIVES QUARTILES
  /MISSING LISTWISE
  /METHOD=EXACT TIMER(1).
Chi2(1,5)=53.62,p=.000.
NPAR TESTS
  /WILCOXON=NrErr1 NrErr1 NrErr1 NrErr1 NrErr1
    NrErr2 NrErr2 NrErr2 NrErr2
    NrErr3 NrErr3 NrErr3
    NrErr4 NrErr4 NrErr5 WITH
    NrFix2 NrFix3 NrFix4 NrFix5 NrFix6
    NrErr3 NrErr4 NrErr5 NrErr6
    NrErr4 NrErr5 NrErr6
    NrErr5 NrErr6 NrErr6 (PAIRED)
  /STATISTICS DESCRIPTIVES QUARTILES
  /MISSING ANALYSIS
  /METHOD=EXACT TIMER(1).
*1:2-6 p<.000,2:3-6:p<.05,3:6:p<.01,4:5-6:p<.05,5:6:n.s.

*ErrDist.
GLM ErrDist1 ErrDist2 ErrDist3 ErrDist4 ErrDist5 ErrDist6
  /WSFACTOR=block 6 Polynomial 
  /METHOD=SSTYPE(3)
  /PLOT=PROFILE(block)
  /EMMEANS=TABLES(block) COMPARE ADJ(LSD)
  /PRINT=DESCRIPTIVE ETASQ OPOWER PARAMETER 
  /CRITERIA=ALPHA(.05)
  /WSDESIGN=block.
Block*:F(5,110)=3.57,MSE=85.46,p=.022,eta=.14, power=.91, epsiolon=.55
linearTrend:F(1,39)=9.11,MSE=152.37,p=.006,eta=.29,power=.82
*1:3-6 p<.05,2:3-5:n.s.,3:4-6:n.s.,4:5-6:n.s.,5:6:n.s.p=.422.

*NrFix.
GLM NrFix1 NrFix2 NrFix3 NrFix4 NrFix5 NrFix6
  /WSFACTOR=block 6 Polynomial 
  /METHOD=SSTYPE(3)
  /PLOT=PROFILE(block)
  /EMMEANS=TABLES(block) COMPARE ADJ(LSD)
  /PRINT=DESCRIPTIVE ETASQ OPOWER PARAMETER 
  /CRITERIA=ALPHA(.05)
  /WSDESIGN=block.
Block*:F(5,195)=55.39,MSE=276.66,p=.000,eta=.59, power=1.00, epsilon=.62
linearTrend:F(1,39)=82.16,MSE=542.70,p=.000,eta=.68,power=1.00
*1:2-6 p<.000,2:4-6:p<.01,3:4:p<.05,4:5-6:n.s.,5:6:n.s.p=.565.

*FixDur.
GLM FixDur1 FixDur2 FixDur3 FixDur4 FixDur5 FixDur6
  /WSFACTOR=block 6 Polynomial 
  /METHOD=SSTYPE(3)
  /PLOT=PROFILE(block)
  /EMMEANS=TABLES(block) COMPARE ADJ(LSD)
  /PRINT=DESCRIPTIVE ETASQ OPOWER PARAMETER 
  /CRITERIA=ALPHA(.05)
  /WSDESIGN=block.
Block*:F(5,195)=3.27,MSE=12465.64,p=.035,eta=.08, power=.66, epsilon=.47
linearTrend:F(1,39)=1.00,MSE=1284.53,p=.323,eta=.03,power=.16
quadraticTrend*:F(1,29)=5.08,MSE=16889.52,p=.030,eta=.12,power=.60
1:2-6:p<.05,2:3-6:n.s.,3:4-6:n.s.,4:5-6:n.s.,5:6:n.s.p=.295.

*Cpath.
GLM Cpath1 Cpath2 Cpath3 Cpath4 Cpath5 Cpath6
  /WSFACTOR=block 6 Polynomial 
  /METHOD=SSTYPE(3)
  /PLOT=PROFILE(block)
  /EMMEANS=TABLES(block) COMPARE ADJ(LSD)
  /PRINT=DESCRIPTIVE ETASQ OPOWER PARAMETER 
  /CRITERIA=ALPHA(.05)
  /WSDESIGN=block.
Block*:F(5,195)=52.77,MSE=6292.82,p=.000,eta=.58, power=1.00, epsilon=.71
linearTrend:F(1,39)=83.44,MSE=14611.89,p=.000,eta=.68,power=1.00
1:2-6:p<.001,2:3-6:p<.01,3:4:p<.01,4:5-6:n.s.,5:6:n.s.p=.714.

*Epath.
GLM Epath1 Epath2 Epath3 Epath4 Epath5 Epath6
  /WSFACTOR=block 6 Polynomial 
  /METHOD=SSTYPE(3)
  /PLOT=PROFILE(block)
  /EMMEANS=TABLES(block) COMPARE ADJ(LSD)
  /PRINT=DESCRIPTIVE ETASQ OPOWER PARAMETER 
  /CRITERIA=ALPHA(.05)
  /WSDESIGN=block.
Block*:F(5,195)=49.03,MSE=15680.52,p=.000,eta=.56,power=1.00, epsilon=.67
linearTrend:F(1,39)=75.67,MSE=30448.98,p=.000,eta=.66,power=1.00
1:2-6:p<.001,2:3-5:p<.05,3:4-6:n.s.,4:5-6:n.s.,5:6:n.s.p=.788.

*ECdist.
GLM ECdist1 ECdist2 ECdist3 ECdist4 ECdist5 ECdist6
  /WSFACTOR=block 6 Polynomial 
  /METHOD=SSTYPE(3)
  /PLOT=PROFILE(block)
  /EMMEANS=TABLES(block) COMPARE ADJ(LSD)
  /PRINT=DESCRIPTIVE ETASQ OPOWER PARAMETER 
  /CRITERIA=ALPHA(.05)
  /WSDESIGN=block.
Block*:F(5,195)=5.22,MSE=2.51,p=.004,eta=.12,power=.88, epsilon=.50
linearTrend:F(1,39)=16.99,MSE=5.2,p=.000,eta=.30,power=.98
1:2-6:p<.05,2:3-6:n.s.,3:4-6:n.s.,4:5-6:n.s.,5:6:n.s.p=.582.



*******************************************************Change effects*******************************************************
*******************************************************Trial by Group Effects.
*load data.
GET DATA  /TYPE=TXT
  /FILE="E:\Experimente\NCT\NCT_Results\NCT_ExpectationDiscrepancy\ExpectDiscrep_V8\results\SPSStabs\"
+"main_block.txt"
  /ENCODING='Locale'
  /DELCASE=LINE
  /DELIMITERS="\t"
  /ARRANGEMENT=DELIMITED
  /FIRSTCASE=2
  /IMPORTCASE=ALL
  /VARIABLES=
  Group F1.0
  Subject F2.0
  TimePre F11.7
  TimeCha1 F11.7
  TimeCha2 F11.7
  TimeCha3 F11.7
  TimeCha4 F11.7
  TimeCha5 F11.7
  TimeCha6 F11.7
  TimeCha7 F11.7
  TimeCha8 F11.7
  TimeCha9 F11.7
  TimeCha10 F11.7
  TimeCha11 F11.7
  TimeCha12 F11.7
  TimeCha13 F11.7
  TimeCha14 F11.7
  TimeCha15 F11.7
  TimeCha16 F11.7
  TimeCha17 F11.7
  TimeCha18 F11.7
  TimeCha19 F11.7
  TimeCha20 F11.7
  TimeRev1 F11.7
  TimeRev2 F11.7
  TimeRev3 F11.7
  TimeRev4 F11.7
  TimeRev5 F11.7
  TimeRev6 F11.7
  TimeRev7 F11.7
  TimeRev8 F11.7
  TimeRev9 F11.7
  TimeRev10 F11.7
  TimeRev11 F11.7
  TimeRev12 F11.7
  TimeRev13 F11.7
  TimeRev14 F11.7
  TimeRev15 F11.7
  TimeRev16 F11.7
  TimeRev17 F11.7
  TimeRev18 F11.7
  TimeRev19 F11.7
  TimeRev20 F11.7
  NrErrPre F11.7
  NrErrCha1 F11.7
  NrErrCha2 F11.7
  NrErrCha3 F11.7
  NrErrCha4 F11.7
  NrErrCha5 F11.7
  NrErrCha6 F11.7
  NrErrCha7 F11.7
  NrErrCha8 F11.7
  NrErrCha9 F11.7
  NrErrCha10 F11.7
  NrErrCha11 F11.7
  NrErrCha12 F11.7
  NrErrCha13 F11.7
  NrErrCha14 F11.7
  NrErrCha15 F11.7
  NrErrCha16 F11.7
  NrErrCha17 F11.7
  NrErrCha18 F11.7
  NrErrCha19 F11.7
  NrErrCha20 F11.7
  NrErrRev1 F11.7
  NrErrRev2 F11.7
  NrErrRev3 F11.7
  NrErrRev4 F11.7
  NrErrRev5 F11.7
  NrErrRev6 F11.7
  NrErrRev7 F11.7
  NrErrRev8 F11.7
  NrErrRev9 F11.7
  NrErrRev10 F11.7
  NrErrRev11 F11.7
  NrErrRev12 F11.7
  NrErrRev13 F11.7
  NrErrRev14 F11.7
  NrErrRev15 F11.7
  NrErrRev16 F11.7
  NrErrRev17 F11.7
  NrErrRev18 F11.7
  NrErrRev19 F11.7
  NrErrRev20 F11.7
  ErrDistPre F11.7
  ErrDistCha1 F11.7
  ErrDistCha2 F11.7
  ErrDistCha3 F11.7
  ErrDistCha4 F11.7
  ErrDistCha5 F11.7
  ErrDistCha6 F11.7
  ErrDistCha7 F11.7
  ErrDistCha8 F11.7
  ErrDistCha9 F11.7
  ErrDistCha10 F11.7
  ErrDistCha11 F11.7
  ErrDistCha12 F11.7
  ErrDistCha13 F11.7
  ErrDistCha14 F11.7
  ErrDistCha15 F11.7
  ErrDistCha16 F11.7
  ErrDistCha17 F11.7
  ErrDistCha18 F11.7
  ErrDistCha19 F11.7
  ErrDistCha20 F11.7
  ErrDistRev1 F11.7
  ErrDistRev2 F11.7
  ErrDistRev3 F11.7
  ErrDistRev4 F11.7
  ErrDistRev5 F11.7
  ErrDistRev6 F11.7
  ErrDistRev7 F11.7
  ErrDistRev8 F11.7
  ErrDistRev9 F11.7
  ErrDistRev10 F11.7
  ErrDistRev11 F11.7
  ErrDistRev12 F11.7
  ErrDistRev13 F11.7
  ErrDistRev14 F11.7
  ErrDistRev15 F11.7
  ErrDistRev16 F11.7
  ErrDistRev17 F11.7
  ErrDistRev18 F11.7
  ErrDistRev19 F11.7
  ErrDistRev20 F11.7
  NrFixPre F11.7
  NrFixCha1 F11.7
  NrFixCha2 F11.7
  NrFixCha3 F11.7
  NrFixCha4 F11.7
  NrFixCha5 F11.7
  NrFixCha6 F11.7
  NrFixCha7 F11.7
  NrFixCha8 F11.7
  NrFixCha9 F11.7
  NrFixCha10 F11.7
  NrFixCha11 F11.7
  NrFixCha12 F11.7
  NrFixCha13 F11.7
  NrFixCha14 F11.7
  NrFixCha15 F11.7
  NrFixCha16 F11.7
  NrFixCha17 F11.7
  NrFixCha18 F11.7
  NrFixCha19 F11.7
  NrFixCha20 F11.7
  NrFixRev1 F11.7
  NrFixRev2 F11.7
  NrFixRev3 F11.7
  NrFixRev4 F11.7
  NrFixRev5 F11.7
  NrFixRev6 F11.7
  NrFixRev7 F11.7
  NrFixRev8 F11.7
  NrFixRev9 F11.7
  NrFixRev10 F11.7
  NrFixRev11 F11.7
  NrFixRev12 F11.7
  NrFixRev13 F11.7
  NrFixRev14 F11.7
  NrFixRev15 F11.7
  NrFixRev16 F11.7
  NrFixRev17 F11.7
  NrFixRev18 F11.7
  NrFixRev19 F11.7
  NrFixRev20 F11.7
  FixDurPre F11.7
  FixDurCha1 F11.7
  FixDurCha2 F11.7
  FixDurCha3 F11.7
  FixDurCha4 F11.7
  FixDurCha5 F11.7
  FixDurCha6 F11.7
  FixDurCha7 F11.7
  FixDurCha8 F11.7
  FixDurCha9 F11.7
  FixDurCha10 F11.7
  FixDurCha11 F11.7
  FixDurCha12 F11.7
  FixDurCha13 F11.7
  FixDurCha14 F11.7
  FixDurCha15 F11.7
  FixDurCha16 F11.7
  FixDurCha17 F11.7
  FixDurCha18 F11.7
  FixDurCha19 F11.7
  FixDurCha20 F11.7
  FixDurRev1 F11.7
  FixDurRev2 F11.7
  FixDurRev3 F11.7
  FixDurRev4 F11.7
  FixDurRev5 F11.7
  FixDurRev6 F11.7
  FixDurRev7 F11.7
  FixDurRev8 F11.7
  FixDurRev9 F11.7
  FixDurRev10 F11.7
  FixDurRev11 F11.7
  FixDurRev12 F11.7
  FixDurRev13 F11.7
  FixDurRev14 F11.7
  FixDurRev15 F11.7
  FixDurRev16 F11.7
  FixDurRev17 F11.7
  FixDurRev18 F11.7
  FixDurRev19 F11.7
  FixDurRev20 F11.7
  CpathPre F11.7
  CpathCha1 F11.7
  CpathCha2 F11.7
  CpathCha3 F11.7
  CpathCha4 F11.7
  CpathCha5 F11.7
  CpathCha6 F11.7
  CpathCha7 F11.7
  CpathCha8 F11.7
  CpathCha9 F11.7
  CpathCha10 F11.7
  CpathCha11 F11.7
  CpathCha12 F11.7
  CpathCha13 F11.7
  CpathCha14 F11.7
  CpathCha15 F11.7
  CpathCha16 F11.7
  CpathCha17 F11.7
  CpathCha18 F11.7
  CpathCha19 F11.7
  CpathCha20 F11.7
  CpathRev1 F11.7
  CpathRev2 F11.7
  CpathRev3 F11.7
  CpathRev4 F11.7
  CpathRev5 F11.7
  CpathRev6 F11.7
  CpathRev7 F11.7
  CpathRev8 F11.7
  CpathRev9 F11.7
  CpathRev10 F11.7
  CpathRev11 F11.7
  CpathRev12 F11.7
  CpathRev13 F11.7
  CpathRev14 F11.7
  CpathRev15 F11.7
  CpathRev16 F11.7
  CpathRev17 F11.7
  CpathRev18 F11.7
  CpathRev19 F11.7
  CpathRev20 F11.7
  EpathPre F11.7
  EpathCha1 F11.7
  EpathCha2 F11.7
  EpathCha3 F11.7
  EpathCha4 F11.7
  EpathCha5 F11.7
  EpathCha6 F11.7
  EpathCha7 F11.7
  EpathCha8 F11.7
  EpathCha9 F11.7
  EpathCha10 F11.7
  EpathCha11 F11.7
  EpathCha12 F11.7
  EpathCha13 F11.7
  EpathCha14 F11.7
  EpathCha15 F11.7
  EpathCha16 F11.7
  EpathCha17 F11.7
  EpathCha18 F11.7
  EpathCha19 F11.7
  EpathCha20 F11.7
  EpathRev1 F11.7
  EpathRev2 F11.7
  EpathRev3 F11.7
  EpathRev4 F11.7
  EpathRev5 F11.7
  EpathRev6 F11.7
  EpathRev7 F11.7
  EpathRev8 F11.7
  EpathRev9 F11.7
  EpathRev10 F11.7
  EpathRev11 F11.7
  EpathRev12 F11.7
  EpathRev13 F11.7
  EpathRev14 F11.7
  EpathRev15 F11.7
  EpathRev16 F11.7
  EpathRev17 F11.7
  EpathRev18 F11.7
  EpathRev19 F11.7
  EpathRev20 F11.7
  ECdistPre F11.7
  ECdistCha1 F11.7
  ECdistCha2 F11.7
  ECdistCha3 F11.7
  ECdistCha4 F11.7
  ECdistCha5 F11.7
  ECdistCha6 F11.7
  ECdistCha7 F11.7
  ECdistCha8 F11.7
  ECdistCha9 F11.7
  ECdistCha10 F11.7
  ECdistCha11 F11.7
  ECdistCha12 F11.7
  ECdistCha13 F11.7
  ECdistCha14 F11.7
  ECdistCha15 F11.7
  ECdistCha16 F11.7
  ECdistCha17 F11.7
  ECdistCha18 F11.7
  ECdistCha19 F11.7
  ECdistCha20 F11.7
  ECdistRev1 F11.7
  ECdistRev2 F11.7
  ECdistRev3 F11.7
  ECdistRev4 F11.7
  ECdistRev5 F11.7
  ECdistRev6 F11.7
  ECdistRev7 F11.7
  ECdistRev8 F11.7
  ECdistRev9 F11.7
  ECdistRev10 F11.7
  ECdistRev11 F11.7
  ECdistRev12 F11.7
  ECdistRev13 F11.7
  ECdistRev14 F11.7
  ECdistRev15 F11.7
  ECdistRev16 F11.7
  ECdistRev17 F11.7
  ECdistRev18 F11.7
  ECdistRev19 F11.7
  ECdistRev20 F11.7
  NrSearchPre F11.7
  NrSearchCha1 F11.7
  NrSearchCha2 F11.7
  NrSearchCha3 F11.7
  NrSearchCha4 F11.7
  NrSearchCha5 F11.7
  NrSearchCha6 F11.7
  NrSearchCha7 F11.7
  NrSearchCha8 F11.7
  NrSearchCha9 F11.7
  NrSearchCha10 F11.7
  NrSearchCha11 F11.7
  NrSearchCha12 F11.7
  NrSearchCha13 F11.7
  NrSearchCha14 F11.7
  NrSearchCha15 F11.7
  NrSearchCha16 F11.7
  NrSearchCha17 F11.7
  NrSearchCha18 F11.7
  NrSearchCha19 F11.7
  NrSearchCha20 F11.7
  NrSearchRev1 F11.7
  NrSearchRev2 F11.7
  NrSearchRev3 F11.7
  NrSearchRev4 F11.7
  NrSearchRev5 F11.7
  NrSearchRev6 F11.7
  NrSearchRev7 F11.7
  NrSearchRev8 F11.7
  NrSearchRev9 F11.7
  NrSearchRev10 F11.7
  NrSearchRev11 F11.7
  NrSearchRev12 F11.7
  NrSearchRev13 F11.7
  NrSearchRev14 F11.7
  NrSearchRev15 F11.7
  NrSearchRev16 F11.7
  NrSearchRev17 F11.7
  NrSearchRev18 F11.7
  NrSearchRev19 F11.7
  NrSearchRev20 F11.7.
CACHE.
COMPUTE TimeDiff=TimeCha1-TimePre.
COMPUTE NrErrDiff=NrErrCha1-NrErrPre.
COMPUTE ErrDistDiff=ErrDistCha1-ErrDistPre.
COMPUTE NrFixDiff=NrFixCha1-NrFixPre.
COMPUTE FixDurDiff=FixDurCha1-FixDurPre.
COMPUTE CpathDiff=CpathCha1-CpathPre.
COMPUTE EpathDiff=EpathCha1-EpathPre.
COMPUTE ECdistDiff=ECdistCha1-ECdistPre.
EXECUTE.
DATASET NAME main WINDOW=FRONT.

*******************Test for normal distribution*******************
*Test for normal distribution per group.
DATASET ACTIVATE main.
SPLIT FILE SEPARATE BY Group.
NPAR TESTS
  /K-S(NORMAL)=
    TimePre TimeCha1 
    NrErrPre NrErrCha1
    ErrDistPre ErrDistCha1
    NrFixPre NrFixCha1
    FixDurPre FixDurCha1
    CpathPre CpathCha1
    EpathPre EpathCha1
    ECdistPre ECdistCha1
    NrSearchPre NrSearchCha1
  /STATISTICS DESCRIPTIVES
  /MISSING ANALYSIS
  /METHOD=EXACT TIMER(1).
**main_block baseline.
*NotNormal(according to exact ps):
Gr1ShapeChange:all normal
Gr2NumberChange:all normal
Gr3NumberShapeChange: NrErrPreCha1
Gr4Control: all normal.

*******************Baseline differences*******************
*TimeTrial.
DATASET ACTIVATE main.
SPLIT FILE OFF.
UNIANOVA TimePre BY Group
  /METHOD=SSTYPE(3)
  /INTERCEPT=INCLUDE
  /POSTHOC=Group(LSD) 
  /PLOT=PROFILE(Group)
  /EMMEANS=TABLES(Group) COMPARE ADJ(LSD)
  /PRINT=OPOWER ETASQ HOMOGENEITY DESCRIPTIVE PARAMETER
  /PLOT=RESIDUALS
  /CRITERIA=ALPHA(.05)
  /DESIGN=Group.
**main_block baseline.
*Levene p=.014
F(3,36)=2.15,p=.112,eta=.15,power=.50.

*NrErr.
UNIANOVA NrErrPre BY Group
  /METHOD=SSTYPE(3)
  /INTERCEPT=INCLUDE
  /POSTHOC=Group(LSD) 
  /PLOT=PROFILE(Group)
  /EMMEANS=TABLES(Group) COMPARE ADJ(LSD)
  /PRINT=OPOWER ETASQ HOMOGENEITY DESCRIPTIVE PARAMETER
  /PLOT=RESIDUALS
  /CRITERIA=ALPHA(.05)
  /DESIGN=Group.
**main_block baseline.
*Levene p=.496
F(3,36)=.15,p=.931,eta=.01,power=.07.

*ErrDist.
UNIANOVA ErrDistPre BY Group
  /METHOD=SSTYPE(3)
  /INTERCEPT=INCLUDE
  /POSTHOC=Group(LSD) 
  /PLOT=PROFILE(Group)
  /EMMEANS=TABLES(Group) COMPARE ADJ(LSD)
  /PRINT=OPOWER ETASQ HOMOGENEITY DESCRIPTIVE PARAMETER
  /PLOT=RESIDUALS
  /CRITERIA=ALPHA(.05)
  /DESIGN=Group.
**main_block baseline.
*Levene p=.045
F(3,30)=2.37,p=.090,eta=.19,power=.54.

*FixDur.
UNIANOVA FixDurPre BY Group
  /METHOD=SSTYPE(3)
  /INTERCEPT=INCLUDE
  /POSTHOC=Group(LSD) 
  /PLOT=PROFILE(Group)
  /EMMEANS=TABLES(Group) COMPARE ADJ(LSD)
  /PRINT=OPOWER ETASQ HOMOGENEITY DESCRIPTIVE PARAMETER
  /PLOT=RESIDUALS
  /CRITERIA=ALPHA(.05)
  /DESIGN=Group.
**main_block baseline.
*Levene p=.077
F(3,36)=.74,p=.534,eta=.06,power=.19.

*Cpath.
UNIANOVA CpathPre BY Group
  /METHOD=SSTYPE(3)
  /INTERCEPT=INCLUDE
  /POSTHOC=Group(LSD) 
  /PLOT=PROFILE(Group)
  /EMMEANS=TABLES(Group) COMPARE ADJ(LSD)
  /PRINT=OPOWER ETASQ HOMOGENEITY DESCRIPTIVE PARAMETER
  /PLOT=RESIDUALS
  /CRITERIA=ALPHA(.05)
  /DESIGN=Group.
**main_block baseline.
*Levene p=.669
F(3,36)=.73,p=.543,eta=.06,power=.19.

*Epath.
UNIANOVA EpathPre BY Group
  /METHOD=SSTYPE(3)
  /INTERCEPT=INCLUDE
  /POSTHOC=Group(LSD) 
  /PLOT=PROFILE(Group)
  /EMMEANS=TABLES(Group) COMPARE ADJ(LSD)
  /PRINT=OPOWER ETASQ HOMOGENEITY DESCRIPTIVE PARAMETER
  /PLOT=RESIDUALS
  /CRITERIA=ALPHA(.05)
  /DESIGN=Group.
**main_block baseline.
*Levene p=.060
F(3,36)=2.59,p=.068,eta=.18,power=.59.

*ECdist.
UNIANOVA ECdistPre BY Group
  /METHOD=SSTYPE(3)
  /INTERCEPT=INCLUDE
  /POSTHOC=Group(LSD) 
  /PLOT=PROFILE(Group)
  /EMMEANS=TABLES(Group) COMPARE ADJ(LSD)
  /PRINT=OPOWER ETASQ HOMOGENEITY DESCRIPTIVE PARAMETER
  /PLOT=RESIDUALS
  /CRITERIA=ALPHA(.05)
  /DESIGN=Group.
**main_block baseline.
*Levene p=.970
F(3,36)=3.22,p=.034,eta=.21,power=.69.


*******************Cha1effects*******************
*TimeTrial.
DATASET ACTIVATE main.
SPLIT FILE OFF.
GLM TimePre TimeCha1 BY Group
  /WSFACTOR=condition 2 Polynomial 
  /METHOD=SSTYPE(3)
  /POSTHOC=Group(LSD) 
  /PLOT=PROFILE(Group*condition)
  /EMMEANS=TABLES(Group) COMPARE ADJ(LSD)
  /EMMEANS=TABLES(condition) COMPARE ADJ(LSD)
  /EMMEANS=TABLES(Group*condition) 
  /EMMEANS=TABLES(OVERALL) 
  /PRINT=DESCRIPTIVE ETASQ OPOWER PARAMETER HOMOGENEITY 
  /CRITERIA=ALPHA(.05)
  /WSDESIGN=condition 
  /DESIGN=Group.
**main_block baseline
Condition*:F(1,36)=13.47,MSE=23.03,p=.001,eta=.27,power=.95
Group*:F(3,36)=3.64,MSE=10.80,p=.022,eta=.23,power=.735
Interaction*:F(3,36)=3.50,MSE=5.98,p=.025,eta=.23,power=.73.
DATASET ACTIVATE main.
SPLIT FILE SEPARATE BY Group.
T-TEST PAIRS=TimePre WITH TimeCha1 (PAIRED)
  /CRITERIA=CI(.9500)
  /MISSING=ANALYSIS.
**main_block baseline
Gr1:t(9)=.63,SE=.24,p=.547
Gr2*:t(9)=3.03,SE=.58,p=.014
Gr3*:t(9)=2.57,SE=.88,p=.030
Gr4:t(9)=.31,SE=.45,p=.765.

*NrError.
DATASET ACTIVATE main.
SPLIT FILE OFF.
GLM NrErrPre NrErrCha1 BY Group
  /WSFACTOR=condition 2 Polynomial 
  /METHOD=SSTYPE(3)
  /POSTHOC=Group(LSD) 
  /PLOT=PROFILE(Group*condition)
  /EMMEANS=TABLES(Group) COMPARE ADJ(LSD)
  /EMMEANS=TABLES(condition) COMPARE ADJ(LSD)
  /EMMEANS=TABLES(Group*condition) 
  /EMMEANS=TABLES(OVERALL) 
  /PRINT=DESCRIPTIVE ETASQ OPOWER PARAMETER HOMOGENEITY 
  /CRITERIA=ALPHA(.05)
  /WSDESIGN=condition 
  /DESIGN=Group.
**main_block baseline
Condition:F(1,36)=1.00,MSE=1.95,p=.323,eta=.03,power=.16
Group:F(3,36)=.06,MSE=.23,p=.981,eta=.01,power=.06
Interaction(3,36):F=.10,MSE=.20,p=.958,eta=.01,power=.07.
DATASET ACTIVATE main.
SPLIT FILE SEPARATE BY Group.
T-TEST PAIRS=NrErrPre WITH NrErrCha1 (PAIRED)
  /CRITERIA=CI(.9500)
  /MISSING=ANALYSIS.
**main_block baseline
Gr1:t(9)=.33,SE=.33,p=.749
Gr2:t(9)=.39,SE=.49,p=.706
Gr3:t(9)=.57,SE=.96,p=.580
Gr4:t(9)=.74,SE=.54,p=.477.

*ErrDist.
DATASET ACTIVATE main.
SPLIT FILE OFF.
GLM ErrDistPre ErrDistCha1 BY Group
  /WSFACTOR=condition 2 Polynomial 
  /METHOD=SSTYPE(3)
  /POSTHOC=Group(LSD) 
  /PLOT=PROFILE(Group*condition)
  /EMMEANS=TABLES(Group) COMPARE ADJ(LSD)
  /EMMEANS=TABLES(condition) COMPARE ADJ(LSD)
  /EMMEANS=TABLES(Group*condition) 
  /EMMEANS=TABLES(OVERALL) 
  /PRINT=DESCRIPTIVE ETASQ OPOWER PARAMETER HOMOGENEITY 
  /CRITERIA=ALPHA(.05)
  /WSDESIGN=condition 
  /DESIGN=Group.
**main_block baseline
Condition*:F(1,11)=24.03,MSE=216.46,p=.000,eta=.69,power=.99
Group*:F(3,11)=5.41,MSE=94.00,p=.016,eta=.60,power=.82
Interaction*(3,11):F=12.99,MSE=117.03,p=.001,eta=.78,power=1.00.
DATASET ACTIVATE main.
SPLIT FILE SEPARATE BY Group.
T-TEST PAIRS=ErrDistPre WITH ErrDistCha1 (PAIRED)
  /CRITERIA=CI(.9500)
  /MISSING=ANALYSIS.
**main_block baseline
Gr1:t(4)=.74,SE=.54,p=.477
Gr2*:t(4)=6.18,SE=2.50,p=.003
Gr4:t(3)=1.82,SE=2.41,p=.167.

*NrFix.
DATASET ACTIVATE main.
SPLIT FILE OFF.
GLM NrFixPre NrFixCha1 BY Group
  /WSFACTOR=condition 2 Polynomial 
  /METHOD=SSTYPE(3)
  /POSTHOC=Group(LSD) 
  /PLOT=PROFILE(Group*condition)
  /EMMEANS=TABLES(Group) COMPARE ADJ(LSD)
  /EMMEANS=TABLES(condition) COMPARE ADJ(LSD)
  /EMMEANS=TABLES(Group*condition) 
  /EMMEANS=TABLES(OVERALL) 
  /PRINT=DESCRIPTIVE ETASQ OPOWER PARAMETER HOMOGENEITY 
  /CRITERIA=ALPHA(.05)
  /WSDESIGN=condition 
  /DESIGN=Group.
**main_block baseline
Condition*:F(1,36)=25.63,MSE=607.75,p=.000,eta=.42, power=1.00
Group*:F(3,36)=6.19,MSE=269.64,p=.002,eta=.34, power=.94
Interaction*:F(3,36)=7.05,MSE=167.07,p=.001,eta=.37, power=.97.
DATASET ACTIVATE main.
SPLIT FILE SEPARATE BY Group.
T-TEST PAIRS=NrFixPre WITH NrFixCha1 (PAIRED)
  /CRITERIA=CI(.9500)
  /MISSING=ANALYSIS.
**main_trial baseline
Gr1:t(9)=1.04,SE=.88,p=.325
Gr2*:t(9)=4.16,SE=1.90,p=.002
Gr3*:t(9)=3.49,SE=3.60,p=.007
Gr4:t(9)=.52,SE=1.27,p=.615.

*FixDur.
DATASET ACTIVATE main.
SPLIT FILE OFF.
GLM FixDurPre FixDurCha1 BY Group
  /WSFACTOR=condition 2 Polynomial 
  /METHOD=SSTYPE(3)
  /POSTHOC=Group(LSD) 
  /PLOT=PROFILE(Group*condition)
  /EMMEANS=TABLES(Group) COMPARE ADJ(LSD)
  /EMMEANS=TABLES(condition) COMPARE ADJ(LSD)
  /EMMEANS=TABLES(Group*condition) 
  /EMMEANS=TABLES(OVERALL) 
  /PRINT=DESCRIPTIVE ETASQ OPOWER PARAMETER HOMOGENEITY 
  /CRITERIA=ALPHA(.05)
  /WSDESIGN=condition 
  /DESIGN=Group.
**main_block baseline
Condition*:F(1,36)=32.62,MSE=21264.46,p=.000,eta=.48,power=1.00
Group:F(3,36)=2.13,MSE=13184.44,p=.114,eta=.15,power=.50
Interaction*:F(3,36)=9.28,MSE=6050.72,p=.000,eta=.44, power=.99.
DATASET ACTIVATE main.
SPLIT FILE SEPARATE BY Group.
T-TEST PAIRS=FixDurPre WITH FixDurCha1 (PAIRED)
  /CRITERIA=CI(.9500)
  /MISSING=ANALYSIS.
**main_block baseline
Gr1:t(9)=.62,SE=11.73,p=.553
Gr2*:t(9)=3.85,SE=10.65,p=.004
Gr3*:t(9)=6.68,SE=11.72,p=.000
Gr4:t(9)=.34,SE=11.53,p=.744.

*Cpath.
DATASET ACTIVATE main.
SPLIT FILE OFF.
GLM CpathPre CpathCha1 BY Group
  /WSFACTOR=condition 2 Polynomial 
  /METHOD=SSTYPE(3)
  /POSTHOC=Group(LSD) 
  /PLOT=PROFILE(Group*condition)
  /EMMEANS=TABLES(Group) COMPARE ADJ(LSD)
  /EMMEANS=TABLES(condition) COMPARE ADJ(LSD)
  /EMMEANS=TABLES(Group*condition) 
  /EMMEANS=TABLES(OVERALL) 
  /PRINT=DESCRIPTIVE ETASQ OPOWER PARAMETER HOMOGENEITY 
  /CRITERIA=ALPHA(.05)
  /WSDESIGN=condition 
  /DESIGN=Group.
**main_block baseline
Condition*:F(1,36)=18.87,MSE=15345.67,p=.000,eta=.34, power=.99
Group*:F(3,36)=6.74,MSE=5951.30,p=.001,eta=.36, power=.96
Interaction*:F(3,36)=5.39,MSE=4383.51,p=.004,eta=.31, power=.91.
DATASET ACTIVATE main.
SPLIT FILE SEPARATE BY Group.
T-TEST PAIRS=CpathPre WITH CpathCha1 (PAIRED)
  /CRITERIA=CI(.9500)
  /MISSING=ANALYSIS.
**main_block baseline
Gr1:t(9)=.33,SE=6.21,p=.750
Gr2*:t(9)=2.73,SE=17.75,p=.023
Gr3*:t(9)=3.88,SE=14.93,p=.004
Gr4:t(9)=.29,SE=8.60,p=.776.

*Epath.
DATASET ACTIVATE main.
SPLIT FILE OFF.
GLM EpathPre EpathCha1 BY Group
  /WSFACTOR=condition 2 Polynomial 
  /METHOD=SSTYPE(3)
  /POSTHOC=Group(LSD) 
  /PLOT=PROFILE(Group*condition)
  /EMMEANS=TABLES(Group) COMPARE ADJ(LSD)
  /EMMEANS=TABLES(condition) COMPARE ADJ(LSD)
  /EMMEANS=TABLES(Group*condition) 
  /EMMEANS=TABLES(OVERALL) 
  /PRINT=DESCRIPTIVE ETASQ OPOWER PARAMETER HOMOGENEITY 
  /CRITERIA=ALPHA(.05)
  /WSDESIGN=condition 
  /DESIGN=Group.
**main_block baseline
Condition*:F(1,36)=29.54,MSE=47492.16,p=.000,eta=.45,power=1.00
Group*:F(3,36)=10.97,MSE=21693.50,p=.000,eta=.48,power=1.00
Interaction*:F(3,36)=8.45,MSE=13588.19,p=.000,eta=.41,power=.99.
DATASET ACTIVATE main.
SPLIT FILE SEPARATE BY Group.
T-TEST PAIRS=EpathPre WITH EpathCha1 (PAIRED)
  /CRITERIA=CI(.9500)
  /MISSING=ANALYSIS.
**main_block baseline
Gr1:t(9)=.79,SE=7.86,p=.451
Gr2*:t(9)=3.69,SE=17.85,p=.005
Gr3*:t(9)=3.98,SE=28.86,p=.003
Gr4:t(9)=.93,SE=8.53,p=.378.

*ECdist.
DATASET ACTIVATE main.
SPLIT FILE OFF.
GLM ECdistPre ECdistCha1 BY Group
  /WSFACTOR=condition 2 Polynomial 
  /METHOD=SSTYPE(3)
  /POSTHOC=Group(LSD) 
  /PLOT=PROFILE(Group*condition)
  /EMMEANS=TABLES(Group) COMPARE ADJ(LSD)
  /EMMEANS=TABLES(condition) COMPARE ADJ(LSD)
  /EMMEANS=TABLES(Group*condition) 
  /EMMEANS=TABLES(OVERALL) 
  /PRINT=DESCRIPTIVE ETASQ OPOWER PARAMETER HOMOGENEITY 
  /CRITERIA=ALPHA(.05)
  /WSDESIGN=condition 
  /DESIGN=Group.
**main_block baseline
Condition*:F(1,36)=31.69,MSE=8.22,p=.000,eta=.47,power=1.00
Group*:F(3,36)=10.07,MSE=9.93,p=.000,eta=.46,power=1.00
Interaction*:F(3,36)=13.26,MSE=3.44,p=.000,eta=.53,power=1.00.
DATASET ACTIVATE main.
SPLIT FILE SEPARATE BY Group.
T-TEST PAIRS=ECdistPre WITH ECdistCha1 (PAIRED)
  /CRITERIA=CI(.9500)
  /MISSING=ANALYSIS.
**main_block baseline
Gr1:t(9)=.80,SE=.14,p=.444
Gr2*:t(9)=6.07,SE=.22,p=.000
Gr3*:t(9)=4.33,SE=.32,p=.002
Gr4:t(9)=.23,SE=.20,p=.824.


*******************UnequalCha1Effects?*******************
*test Diffs gr2 vs. gr3.
DATASET ACTIVATE main.
SPLIT FILE OFF.
T-TEST GROUPS=Group(2 3)
  /MISSING=ANALYSIS
  /VARIABLES=TimeDiff ErrDistDiff NrFixDiff FixDurDiff CpathDiff EpathDiff ECdistDiff
  /CRITERIA=CI(.95).
**main_block baseline
Time:t(18)=.49,SED=1.05,p=.632
ErrDist:TooLessDataPerCell
NrFix:t(18)=1.15,SED=4.07,p=.265
FixDur*:t(18)=2.36,SED=15.84,p=.030
Cpath:t(18)=.41,SED=23.20,p=.687
Epath:t(18)=1.45,SED=49.04,p=.166
ECdist:t(18)=.12,SED=.05,p=.905.

*test Diffs gr2 vs. gr3.
DATASET ACTIVATE  trial.
T-TEST GROUPS=Group(2 3)
  /MISSING=ANALYSIS
  /VARIABLES=TimeCha1 ErrDistCha1 NrFixCha1 FixDurCha1 CpathCha1 EpathCha1 ECdistCha1
  /CRITERIA=CI(.95).
**main_block baseline
Time:t(18)=.93,SED=1.28,p=.278
ErrDist:TooLessDataPerCell
NrFix:t(18)=1.48,SED=4.66,p=.156
FixDur:t(18)=1.71,SED=15.12,p=.104
Cpath:t(18)=.79,SED=21.83,p=.438
Epath:t(18)=1.80,SED=34.65,p=.090
ECdist:t(18)=1.31,SED=.48,p=.207.

*******************ChaEffectDuration*******************
*create all number change group data (2+3).
DATASET ACTIVATE  trial.
DATASET COPY  trialGr23.
DATASET ACTIVATE  trialGr23.
FILTER OFF.
USE ALL.
SELECT IF (Group = 2 | Group = 3).
EXECUTE.

*Test for normal distribution.
DATASET ACTIVATE trialGr23.
NPAR TESTS
  /K-S(NORMAL)=
    TimePre TimeCha1 TimeCha2 TimeCha3 TimeCha4 TimeCha5
    TimeCha6 TimeCha7 TimeCha8 TimeCha9 TimeCha10
    ErrDistPre ErrDistCha1 ErrDistCha2 ErrDistCha3 ErrDistCha4 ErrDistCha5
    ErrDistCha6 ErrDistCha7 ErrDistCha8 ErrDistCha9 ErrDistCha10
    NrFixPre NrFixCha1 NrFixCha2 NrFixCha3 NrFixCha4 NrFixCha5
    NrFixCha6 NrFixCha7 NrFixCha8 NrFixCha9 NrFixCha10
    FixDurPre FixDurCha1 FixDurCha2 FixDurCha3 FixDurCha4 FixDurCha5
    FixDurCha6 FixDurCha7 FixDurCha8 FixDurCha9 FixDurCha10
    CpathPre CpathCha1 CpathCha2 CpathCha3 CpathCha4 CpathCha5
    CpathCha6 CpathCha7 CpathCha8 CpathCha9 CpathCha10
    EpathPre EpathCha1 EpathCha2 EpathCha3 EpathCha4 EpathCha5
    EpathCha6 EpathCha7 EpathCha8 EpathCha9 EpathCha10
    ECdistPre ECdistCha1 ECdistCha2 ECdistCha3 ECdistCha4 ECdistCha5
    ECdistCha6 ECdistCha7 ECdistCha8 ECdistCha9 ECdistCha10
    NrSearchPre NrSearchCha1 NrSearchCha2 NrSearchCha3 NrSearchCha4 NrSearchCha5
    NrSearchCha6 NrSearchCha7 NrSearchCha8 NrSearchCha9 NrSearchCha10
  /STATISTICS DESCRIPTIVES
  /MISSING ANALYSIS
  /METHOD=EXACT TIMER(1).
**main_block baseline
NrSearch67 not normal.

*change effects.
T-TEST PAIRS=TimePre TimePre TimePre TimePre TimePre TimePre TimePre TimePre TimePre TimePre WITH
    TimeCha1 TimeCha2 TimeCha3 TimeCha4 TimeCha5 TimeCha6 TimeCha7 TimeCha8 TimeCha9 TimeCha10 (PAIRED)
  /CRITERIA=CI(.9500)
  /MISSING=ANALYSIS.
**main_block baseline.
*Time:cha1*.001,cha2*.019,cha5*.039.
T-TEST PAIRS=ErrDistPre ErrDistPre ErrDistPre ErrDistPre ErrDistPre ErrDistPre ErrDistPre ErrDistPre ErrDistPre ErrDistPre WITH
    ErrDistCha1 ErrDistCha2 ErrDistCha3 ErrDistCha4 ErrDistCha5 ErrDistCha6 ErrDistCha7 ErrDistCha8 ErrDistCha9 ErrDistCha10 (PAIRED)
  /CRITERIA=CI(.9500)
  /MISSING=ANALYSIS.
**main_block baseline.
*ErrDist:cha1*.002.
T-TEST PAIRS=NrFixPre NrFixPre NrFixPre NrFixPre NrFixPre NrFixPre NrFixPre NrFixPre NrFixPre NrFixPre WITH
    NrFixCha1 NrFixCha2 NrFixCha3 NrFixCha4 NrFixCha5 NrFixCha6 NrFixCha7 NrFixCha8 NrFixCha9 NrFixCha10 (PAIRED)
  /CRITERIA=CI(.9500)
  /MISSING=ANALYSIS.
**main_block baseline.
*NrFix:cha1*.001,cha2*.000,cha3*.003,cha4*.033,cha5*.001.
T-TEST PAIRS=NrSearchPre NrSearchPre NrSearchPre NrSearchPre NrSearchPre NrSearchPre NrSearchPre NrSearchPre NrSearchPre NrSearchPre WITH
    NrSearchCha1 NrSearchCha2 NrSearchCha3 NrSearchCha4 NrSearchCha5 NrSearchCha6 NrSearchCha7 NrSearchCha8 NrSearchCha9 NrSearchCha10 (PAIRED)
  /CRITERIA=CI(.9500)
  /MISSING=ANALYSIS.
**main_block baseline.
*NrSearch:cha1*-cha3.000,cha4*.002,cha5*.003,cha6*.001,cha7*.002,cha8*.012,cha9*.020,cha10*.030.
T-TEST PAIRS=NrSearchPre NrSearchPre NrSearchPre NrSearchPre NrSearchPre NrSearchPre NrSearchPre NrSearchPre NrSearchPre NrSearchPre WITH
    NrSearchCha11 NrSearchCha12 NrSearchCha13 NrSearchCha14 NrSearchCha15 NrSearchCha16 NrSearchCha17 NrSearchCha18 NrSearchCha19 NrSearchCha20 (PAIRED)
  /CRITERIA=CI(.9500)
  /MISSING=ANALYSIS.
**main_block baseline.
*NrSearch:cha11*.015,cha12*.030,cha13*.006,cha14*.041,cha15*.017,cha*19.043.
T-TEST PAIRS=FixDurPre FixDurPre FixDurPre FixDurPre FixDurPre FixDurPre FixDurPre FixDurPre FixDurPre FixDurPre WITH
    FixDurCha1 FixDurCha2 FixDurCha3 FixDurCha4 FixDurCha5 FixDurCha6 FixDurCha7 FixDurCha8 FixDurCha9 FixDurCha10 (PAIRED)
  /CRITERIA=CI(.9500)
  /MISSING=ANALYSIS.
**main_block baseline.
*FixDur:cha1*.000,cha2*.002,cha3*.000,cha4*.001,cha5*.000,cha6*.001,cha7*.001,cha8*.016,cha9*.044.
T-TEST PAIRS=CpathPre CpathPre CpathPre CpathPre CpathPre CpathPre CpathPre CpathPre CpathPre CpathPre WITH
    CpathCha1 CpathCha2 CpathCha3 CpathCha4 CpathCha5 CpathCha6 CpathCha7 CpathCha8 CpathCha9 CpathCha10 (PAIRED)
  /CRITERIA=CI(.9500)
  /MISSING=ANALYSIS.
**main_block baseline.
*Cpath:cha1*.000,cha3*.047.
T-TEST PAIRS=EpathPre EpathPre EpathPre EpathPre EpathPre EpathPre EpathPre EpathPre EpathPre EpathPre WITH
    EpathCha1 EpathCha2 EpathCha3 EpathCha4 EpathCha5 EpathCha6 EpathCha7 EpathCha8 EpathCha9 EpathCha10 (PAIRED)
  /CRITERIA=CI(.9500)
  /MISSING=ANALYSIS.
**main_block baseline.
*Epath:cha1*.000,cha2*.001,cha3*.002,cha5*.002,cha9*.045.
T-TEST PAIRS=ECdistPre ECdistPre ECdistPre ECdistPre ECdistPre ECdistPre ECdistPre ECdistPre ECdistPre ECdistPre WITH
    ECdistCha1 ECdistCha2 ECdistCha3 ECdistCha4 ECdistCha5 ECdistCha6 ECdistCha7 ECdistCha8 ECdistCha9 ECdistCha10(PAIRED)
  /CRITERIA=CI(.9500)
  /MISSING=ANALYSIS.
**main_block baseline.
*ECdist:cha1*-cha10*.<01.
T-TEST PAIRS=ECdistPre ECdistPre ECdistPre ECdistPre ECdistPre ECdistPre ECdistPre ECdistPre ECdistPre ECdistPre WITH
    ECdistCha11 ECdistCha12 ECdistCha13 ECdistCha14 ECdistCha15 ECdistCha16 ECdistCha17 ECdistCha18 ECdistCha19 ECdistCha20 (PAIRED)
  /CRITERIA=CI(.9500)
  /MISSING=ANALYSIS.
**main_block baseline.
*ECdist:cha11*-cha15*<.05.

*******************RevEffects*******************
*Test for normal distribution.
DATASET ACTIVATE trialGr23.
NPAR TESTS
  /K-S(NORMAL)=
    TimePre TimeRev1 TimeRev2 TimeRev3 TimeRev4 TimeRev5
    TimeRev6 TimeRev7 TimeRev8 TimeRev9 TimeRev10
    ErrDistPre ErrDistRev1 ErrDistRev2 ErrDistRev3 ErrDistRev4 ErrDistRev5
    ErrDistRev6 ErrDistRev7 ErrDistRev8 ErrDistRev9 ErrDistRev10
    NrFixPre NrFixRev1 NrFixRev2 NrFixRev3 NrFixRev4 NrFixRev5
    NrFixRev6 NrFixRev7 NrFixRev8 NrFixRev9 NrFixRev10
    FixDurPre FixDurRev1 FixDurRev2 FixDurRev3 FixDurRev4 FixDurRev5
    FixDurRev6 FixDurRev7 FixDurRev8 FixDurRev9 FixDurRev10
    CpathPre CpathRev1 CpathRev2 CpathRev3 CpathRev4 CpathRev5
    CpathRev6 CpathRev7 CpathRev8 CpathRev9 CpathRev10
    EpathPre EpathRev1 EpathRev2 EpathRev3 EpathRev4 EpathRev5
    EpathRev6 EpathRev7 EpathRev8 EpathRev9 EpathRev10
    ECdistPre ECdistRev1 ECdistRev2 ECdistRev3 ECdistRev4 ECdistRev5
    ECdistRev6 ECdistRev7 ECdistRev8 ECdistRev9 ECdistRev10
    NrSearchPre NrSearchRev1 NrSearchRev2 NrSearchRev3 NrSearchRev4 NrSearchRev5
    NrSearchRev6 NrSearchRev7 NrSearchRev8 NrSearchRev9 NrSearchRev10
  /STATISTICS DESCRIPTIVES
  /MISSING ANALYSIS
  /METHOD=EXACT TIMER(1).
**main_block baseline.
*all normal except for ErrdistRev46.

*reversion effects.
T-TEST PAIRS=TimePre TimePre TimePre TimePre TimePre TimePre TimePre TimePre TimePre TimePre WITH
    TimeRev1 TimeRev2 TimeRev3 TimeRev4 TimeRev5 TimeRev6 TimeRev7 TimeRev8 TimeRev9 TimeRev10 (PAIRED)
  /CRITERIA=CI(.9500)
  /MISSING=ANALYSIS.
**main_block baseline.
*Time:rev1*000.
T-TEST PAIRS=ErrDistPre ErrDistPre ErrDistPre ErrDistPre ErrDistPre ErrDistPre ErrDistPre ErrDistPre ErrDistPre ErrDistPre WITH
    ErrDistRev1 ErrDistRev2 ErrDistRev3 ErrDistRev4 ErrDistRev5 ErrDistRev6 ErrDistRev7 ErrDistRev8 ErrDistRev9 ErrDistRev10 (PAIRED)
  /CRITERIA=CI(.9500)
  /MISSING=ANALYSIS.
**main_block baseline.
*ErrDist:rev1*033.
T-TEST PAIRS=NrFixPre NrFixPre NrFixPre NrFixPre NrFixPre NrFixPre NrFixPre NrFixPre NrFixPre NrFixPre WITH
    NrFixRev1 NrFixRev2 NrFixRev3 NrFixRev4 NrFixRev5 NrFixRev6 NrFixRev7 NrFixRev8 NrFixRev9 NrFixRev10 (PAIRED)
  /CRITERIA=CI(.9500)
  /MISSING=ANALYSIS.
**main_block baseline.
*NrFix:rev1*000,rev2*.011,rev3*.036.
T-TEST PAIRS=NrSearchPre NrSearchPre NrSearchPre NrSearchPre NrSearchPre NrSearchPre NrSearchPre NrSearchPre NrSearchPre NrSearchPre WITH
    NrSearchRev1 NrSearchRev2 NrSearchRev3 NrSearchRev4 NrSearchRev5 NrSearchRev6 NrSearchRev7 NrSearchRev8 NrSearchRev9 NrSearchRev10 (PAIRED)
  /CRITERIA=CI(.9500)
  /MISSING=ANALYSIS.
**main_block baseline.
*NrSearch:rev1*000,rev2*.000,rev3*.013,rev4*.013,rev5*.009.
T-TEST PAIRS=FixDurPre FixDurPre FixDurPre FixDurPre FixDurPre FixDurPre FixDurPre FixDurPre FixDurPre FixDurPre WITH
    FixDurRev1 FixDurRev2 FixDurRev3 FixDurRev4 FixDurRev5 FixDurRev6 FixDurRev7 FixDurRev8 FixDurRev9 FixDurRev10 (PAIRED)
  /CRITERIA=CI(.9500)
  /MISSING=ANALYSIS.
**main_block baseline.
*FixDur.rev1*002,rev2*.008,rev3*.016,rev4*.033,rev5*.021.
T-TEST PAIRS=CpathPre CpathPre CpathPre CpathPre CpathPre CpathPre CpathPre CpathPre CpathPre CpathPre WITH
    CpathRev1 CpathRev2 CpathRev3 CpathRev4 CpathRev5 CpathRev6 CpathRev7 CpathRev8 CpathRev9 CpathRev10 (PAIRED)
  /CRITERIA=CI(.9500)
  /MISSING=ANALYSIS.
**main_block baseline.
*Cpath.rev1*000.
T-TEST PAIRS=EpathPre EpathPre EpathPre EpathPre EpathPre EpathPre EpathPre EpathPre EpathPre EpathPre WITH
    EpathRev1 EpathRev2 EpathRev3 EpathRev4 EpathRev5 EpathRev6 EpathRev7 EpathRev8 EpathRev9 EpathRev10 (PAIRED)
  /CRITERIA=CI(.9500)
  /MISSING=ANALYSIS.
**main_block baseline.
*Epath.rev1*000,rev2*.046.
T-TEST PAIRS=ECdistPre ECdistPre ECdistPre ECdistPre ECdistPre ECdistPre ECdistPre ECdistPre ECdistPre ECdistPre WITH
    ECdistRev1 ECdistRev2 ECdistRev3 ECdistRev4 ECdistRev5 ECdistRev6 ECdistRev7 ECdistRev8 ECdistRev9 ECdistRev10 (PAIRED)
  /CRITERIA=CI(.9500)
  /MISSING=ANALYSIS.
**main_block baseline.
*ECdist.rev1-rev5*000,rev7*.005,rev8*.002,rev9*.041.

******************************************************Read type (trials 61-63 vs. 58-60).
*load.
GET DATA  /TYPE=TXT
  /FILE="E:\Experimente\NCT\NCT_Results\NCT_ExpectationDiscrepancy\ExpectDiscrep_V8\results\SPSStabs\"
   +"type_block.txt"
  /ENCODING='Locale'
  /DELCASE=LINE
  /DELIMITERS="\t"
  /ARRANGEMENT=DELIMITED
  /FIRSTCASE=2
  /IMPORTCASE=ALL
  /VARIABLES=
  Subject F2.0
  NrFixPrePrior F11.7
  NrFixPreCurr F11.7
  NrFixPreLater F11.7
  NrFixPreOut F11.7
  NrFixChaPrior F11.7
  NrFixChaCurr F11.7
  NrFixChaLater F11.7
  NrFixChaOut F11.7
  FixDurPrePrior F11.7
  FixDurPreCurr F11.7
  FixDurPreLater F11.7
  FixDurPreOut F11.7
  FixDurChaPrior F11.7
  FixDurChaCurr F11.7
  FixDurChaLater F11.7
  FixDurChaOut F11.7
  SubjectB F2.0
  NrFixDiffPrior F11.7
  NrFixDiffCurr F11.7
  NrFixDiffLater F11.7
  NrfixDiffOut F11.7
  FixDurDiffPrior F11.7
  FixDurDiffCurr F11.7
  FixDurDiffLater F11.7
  FixDurDiffOut F11.7.
CACHE.
EXECUTE.
DATASET NAME type WINDOW=FRONT.

*Test for normal distribution.
NPAR TESTS
  /K-S(NORMAL)=NrFixPrePrior NrFixPreCurr NrFixPreLater NrFixPreOut
    NrFixChaPrior NrFixChaCurr NrFixChaLater NrFixChaOut
    FixDurPrePrior FixDurPreCurr FixDurPreLater FixDurPreOut
    FixDurChaPrior FixDurChaCurr FixDurChaLater FixDurChaOut
  /STATISTICS DESCRIPTIVES QUARTILES
  /MISSING ANALYSIS
  /METHOD=EXACT TIMER(1).
**main_block baseline
Not normal: NrFixChaPrior.
**main_trial baseline
Not normal: NrFixPrePrior, NrFixPreLater, NrFixChaPrior.

*NrFix.
GLM NrFixPrePrior NrFixPreCurr NrFixPreLater NrFixChaPrior NrFixChaCurr NrFixChaLater
  /WSFACTOR=condition 2 Polynomial type 3 Polynomial 
  /METHOD=SSTYPE(3)
  /PLOT=PROFILE(type*condition)
  /EMMEANS=TABLES(OVERALL) 
  /EMMEANS=TABLES(type) COMPARE ADJ(LSD)
  /EMMEANS=TABLES(condition*type) 
  /PRINT=DESCRIPTIVE ETASQ OPOWER PARAMETER 
  /CRITERIA=ALPHA(.05)
  /WSDESIGN=condition type condition*type.
**main_block baseline
condi*:F(1,19)=23.97,MSE=75.53,p=.000,eta=.56,power=1.00
type*:F(2,38)=89.23,MSE=1008.03,p=.000,eta=.82,power=1.00, epsilon=.693
typeXcondi*:F(2,38)=21.49,MSE=80.34,p=.000,eta=.53,power=1.00,epsilon=.774.
**main_trial baseline
condi*:F(1,19)=17.38,MSE=66.01,p=.001,eta=.48,power=.98
type*:F(2,38)=94.20,MSE=1026.70,p=.000,eta=.83,power=1.00, epsilon=.713
typeXcondi*:F(2,38)=26.12,MSE=73.03,p=.000,eta=.58,power=1.00.
T-TEST PAIRS=NrFixPrePrior NrFixPreCurr NrFixPreLater WITH
    NrFixChaPrior NrFixChaCurr NrFixChaLater (PAIRED)
  /CRITERIA=CI(.9500)
  /MISSING=ANALYSIS.
**main_block baseline
Prior:t(19)=1.81,SE=.27,p=.086
Curr:t(19)=.26,SE=.67,p=.797
Later*:t(19)=7.31,SE=.61,p=.000.
**main_trial baseline
Prior:t(19)=1.48,SE=.30,p=.154
Curr:t(19)=.80,SE=.69,p=.435
Later*:t(19)=7.47,SE=.61,p=.000.
NPAR TESTS
  /FRIEDMAN=NrFixDiffPrior NrFixDiffCurr NrFixDiffLater
  /STATISTICS DESCRIPTIVES QUARTILES
  /MISSING LISTWISE
  /METHOD=EXACT TIMER(1).
**main_block baseline
Chi2(1,2;n=20)=16.70,p=.000.
**main_trial baseline
Chi2(1,2;n=20)=29.18,p=.000.
NPAR TESTS
  /WILCOXON=NrFixPrePrior NrFixPreCurr NrFixPreLater WITH
    NrFixChaPrior NrFixChaCurr NrFixChaLater (PAIRED)
  /STATISTICS DESCRIPTIVES QUARTILES
  /MISSING ANALYSIS
  /METHOD=EXACT TIMER(1).
**main_block baseline
Prior:Z(ties=2,total=20)=.94,p=.363
Curr:Z(ties=1,total=20)=.26,p=.806
Later*:Z(ties=0,total=20)=3.85,p=.000.
**main_trial baseline
Prior:Z(ties=10,total=20)=1.41,p=.166
Curr:Z(ties=3,total=20)=.64,p=.545
Later*:Z(ties=1,total=20)=3.81,p=.000.
GLM NrFixPrePrior NrFixPreCurr NrFixPreLater NrFixChaPrior NrFixChaCurr NrFixChaLater
  /WSFACTOR=condition 6 Polynomial 
  /EMMEANS=TABLES(condition) COMPARE ADJ(LSD).

*FixDur.
GLM FixDurPreCurr FixDurPreLater FixDurChaCurr FixDurChaLater
  /WSFACTOR=condition 2 Polynomial type 2 Polynomial 
  /METHOD=SSTYPE(3)
  /PLOT=PROFILE(type*condition)
  /EMMEANS=TABLES(OVERALL) 
  /EMMEANS=TABLES(type) COMPARE ADJ(LSD)
  /EMMEANS=TABLES(condition*type) 
  /PRINT=DESCRIPTIVE ETASQ OPOWER PARAMETER 
  /CRITERIA=ALPHA(.05)
  /WSDESIGN=condition type condition*type.
**main_trial baseline
N=20
Condi:F(1,19)=1.32,MSE=5760.27,p=.265,eta=.07,power=.19
TA*:F(1,19)=31.15,MSE=105336.74,p=.000,eta=.62,power=1.00
TAxCondi:F(1,19)=.30,MSE=1603.31,p=.591,eta=.02,power=.08.
**main_trial baseline
N=8
Condi:F(1,8)=.02,MSE=160.85,p=.889,eta=.003,power=.05
TA*:F(1,8)=8.75,MSE=76660.13,p=.018,eta=.52,power=.74
TAxCondi:F(1,8)=.76,MSE=4155.59,p=.410,eta=.09,power=.12.
T-TEST PAIRS=FixDurPrePrior FixDurPreCurr FixDurPreLater WITH
    FixDurChaPrior FixDurChaCurr FixDurChaLater (PAIRED)
  /CRITERIA=CI(.9500)
  /MISSING=ANALYSIS.
**main_block baseline
Prior:t(5)=.64,SE=69.34,p=.552
Curr:t(19)=2.07,SE=12.54,p=.053
Later:t(19)=.28,SE=28.57,p=.782.
**main_trial baseline
Prior:t(1)=.20,SE=13.75,p=.874
Curr:t(19)=1.32,SE=18.50,p=.203
Later:t(8)=.58,SE=44.71,p=.581.
COMPUTE FixDurCurr = MEAN(FixDurPreCurr,FixDurChaCurr).
COMPUTE FixDurLater = MEAN(FixDurPreLater,FixDurChaLater).
EXECUTE. 
T-TEST PAIRS=FixDurCurr WITH FixDurLater (PAIRED)
  /CRITERIA=CI(.9500)
  /MISSING=ANALYSIS.
**main_block baseline
CurrVSlater*:t(19)=5.58,SE=13.00,p=.000.
**main_trial baseline
CurrVSlater*:t(19)=3.85,SE=21.21,p=.001.
GLM FixDurPreCurr FixDurPreLater FixDurChaCurr FixDurChaLater
  /WSFACTOR=condition 4 Polynomial 
  /EMMEANS=TABLES(condition) COMPARE ADJ(LSD).

*******************************************************Effects per Loc.
*load data.
GET DATA  /TYPE=TXT
  /FILE="E:\Experimente\NCT\NCT_Results\NCT_ExpectationDiscrepancy\ExpectDiscrep_V8\results\SPSStabs\"
   +"loc_block.txt"
  /ENCODING='Locale'
  /DELCASE=LINE
  /DELIMITERS="\t"
  /ARRANGEMENT=DELIMITED
  /FIRSTCASE=2
  /IMPORTCASE=ALL
  /VARIABLES=
  Subject F2.0
  NrSearchPreNo1 F11.9
  NrSearchPreNo2 F11.9
  NrSearchPreNo3 F11.9
  NrSearchPreNo4 F11.9
  NrSearchPreNo5 F11.9
  NrSearchPreNo6 F11.9
  NrSearchPreNo7 F11.9
  NrSearchPreNo8 F11.9
  NrSearchChaNo1 F11.9
  NrSearchChaNo2 F11.9
  NrSearchChaNo3 F11.9
  NrSearchChaNo4 F11.9
  NrSearchChaNo5 F11.9
  NrSearchChaNo6 F11.9
  NrSearchChaNo7 F11.9
  NrSearchChaNo8 F11.9
  NrCheckPreNo1 F11.9
  NrCheckPreNo2 F11.9
  NrCheckPreNo3 F11.9
  NrCheckPreNo4 F11.9
  NrCheckPreNo5 F11.9
  NrCheckPreNo6 F11.9
  NrCheckPreNo7 F11.9
  NrCheckPreNo8 F11.9
  NrCheckChaNo1 F11.9
  NrCheckChaNo2 F11.9
  NrCheckChaNo3 F11.9
  NrCheckChaNo4 F11.9
  NrCheckChaNo5 F11.9
  NrCheckChaNo6 F11.9
  NrCheckChaNo7 F11.9
  NrCheckChaNo8 F11.9
  SubjectB F2.0
  NrSearchDiffNo1 F11.9
  NrSearchDiffNo2 F11.9
  NrSearchDiffNo3 F11.9
  NrSearchDiffNo4 F11.9
  NrSearchDiffNo5 F11.9
  NrSearchDiffNo6 F11.9
  NrSearchDiffNo7 F11.9
  NrSearchDiffNo8 F11.9
  NrCheckDiffNo1 F11.9
  NrCheckDiffNo2 F11.9
  NrCheckDiffNo3 F11.9
  NrCheckDiffNo4 F11.9
  NrCheckDiffNo5 F11.9
  NrCheckDiffNo6 F11.9
  NrCheckDiffNo7 F11.9
  NrCheckDiffNo8 F11.9.
CACHE.
EXECUTE.
DATASET NAME loc WINDOW=FRONT.

*Test for normal distribution.
DATASET ACTIVATE loc.
NPAR TESTS
  /K-S(NORMAL)=
    NrSearchPreNo1 NrSearchPreNo2 NrSearchPreNo3 NrSearchPreNo4
    NrSearchPreNo5 NrSearchPreNo6 NrSearchPreNo7 NrSearchPreNo8
    NrSearchChaNo1 NrSearchChaNo2 NrSearchChaNo3 NrSearchChaNo4
    NrSearchChaNo5 NrSearchChaNo6 NrSearchChaNo7 NrSearchChaNo8
    NrCheckPreNo1 NrCheckPreNo2 NrCheckPreNo3 NrCheckPreNo4
    NrCheckPreNo5 NrCheckPreNo6 NrCheckPreNo7 NrCheckPreNo8
    NrCheckChaNo1 NrCheckChaNo2 NrCheckChaNo3 NrCheckChaNo4
    NrCheckChaNo5 NrCheckChaNo6 NrCheckChaNo7 NrCheckChaNo8
  /STATISTICS DESCRIPTIVES QUARTILES
  /MISSING ANALYSIS
  /METHOD=EXACT TIMER(1).
**main_block baseline
Not normal: NrSearchPreNo24678, NrSearchChaNo237, all NrChecks!
**main_trial baseline
Not normal: NrSearchPreNo234578, NrSearchChaNo237

*NrSearch.
*Parametric.
GLM NrSearchPreNo2 NrSearchPreNo3 NrSearchPreNo4 NrSearchPreNo5 NrSearchPreNo6 
    NrSearchPreNo7 NrSearchPreNo8 NrSearchChaNo2 NrSearchChaNo3 NrSearchChaNo4 
    NrSearchChaNo5 NrSearchChaNo6 NrSearchChaNo7 NrSearchChaNo8
  /WSFACTOR=condi 2 Polynomial loc 7 Polynomial 
  /METHOD=SSTYPE(3)
  /PLOT=PROFILE(loc*condi)
  /EMMEANS=TABLES(condi) 
  /EMMEANS=TABLES(loc) 
  /EMMEANS=TABLES(condi*loc) 
  /PRINT=DESCRIPTIVE ETASQ OPOWER PARAMETER 
  /CRITERIA=ALPHA(.05)
  /WSDESIGN=condi loc condi*loc.
**main_block baseline
condi*:F(1,19)=44.80,MSE=26.41,p=.000,eta=.70,power=1.00
loc*:F(6.114)=7.32,MSE=6.29,p=.000,eta=.28,power=.98, epsilon=.525
condiXNo*:F(6,114)=5.80,MSE=6.40,p=.002,eta=.23,power=.92, epsilon=.453.
**main_trial baseline
condi*:F(1,19)=44.24,MSE=27.03,p=.000,eta=.70,power=1.00
loc*:F(6.114)=6.53,MSE=5.29,p=.000,eta=.26,power=.98, epsilon=.599
condiXNo*:F(6,114)=5.61,MSE=6.86,p=.003,eta=.23,power=.91, epsilon=.448.
T-TEST PAIRS=NrSearchPreNo2 NrSearchPreNo3 NrSearchPreNo4 
    NrSearchPreNo5 NrSearchPreNo6 NrSearchPreNo7 NrSearchPreNo8 WITH
    NrSearchChaNo2 NrSearchChaNo3 NrSearchChaNo4 
    NrSearchChaNo5 NrSearchChaNo6 NrSearchChaNo7 NrSearchChaNo8 (PAIRED)
  /CRITERIA=CI(.9500)
  /MISSING=ANALYSIS.
**main_block baseline
No2:t(19)=.72,SE=.06,p=.483
No3:t(19)=1.21,SE=.07,p=.240
No4*:t(19)=3.58,SE=.20,p=.002
No5*:t(19)=4.71,SE=.22,p=.000
No6*:t(19)=3.60,SE=.39,p=.002
No7(*):t(19)=1.99,SE=.25,p=.061
No8*:t(19)=3.54,SE=.22,p=.002.
**main_trial baseline
No2:t(19)=.57,SE=.09,p=.577
No3:t(19)=1.00,SE=.10,p=.330
No4*:t(19)=4.00,SE=.20,p=.001
No5*:t(19)=4.16,SE=.24,p=.001
No6*:t(19)=3.68,SE=.39,p=.002
No7*:t(19)=2.24,SE=.25,p=.037
No8*:t(19)=2.90,SE=.24,p=.009.
*Non-Parametric.
NPAR TESTS
  /FRIEDMAN=NrSearchDiffNo2 NrSearchDiffNo3 NrSearchDiffNo4
    NrSearchDiffNo5 NrSearchDiffNo6 NrSearchDiffNo7 NrSearchDiffNo8
  /STATISTICS DESCRIPTIVES QUARTILES
  /MISSING LISTWISE
  /METHOD=EXACT TIMER(1).
**main_block baseline
Chi2(1,6;n=20)=33.67,p=.000(asymptotisch).
**main_trial baseline
Chi2(1,6;n=20)=35.70,p=.000(asymptotisch).
NPAR TESTS
  /WILCOXON=NrSearchPreNo2 NrSearchPreNo3 NrSearchPreNo4
    NrSearchPreNo5 NrSearchPreNo6 NrSearchPreNo7 NrSearchPreNo8 WITH
    NrSearchChaNo2 NrSearchChaNo3 NrSearchChaNo4
    NrSearchChaNo5 NrSearchChaNo6 NrSearchChaNo7 NrSearchChaNo8 (PAIRED)
  /STATISTICS DESCRIPTIVES QUARTILES
  /MISSING ANALYSIS
  /METHOD=EXACT TIMER(1).
**main_block baseline
No2:Z(1,ties=11,total=20)=1.61,p=.113
No3*:Z(1,ties=8,total=20)=2.13,p=.029
No4*:Z(1,ties=4,total=20)=2.57,p=.008
No5*:Z(1,ties=2,total=20)=3.60,p=.000
No6*:Z(1,ties=7,total=20)=3.19,p=.000
No7(*):Z(1,ties=7,total=20)=1.72,p=.091
No8*:Z(1,ties=3,total=20)=2.92,p=.002.
**main_trial baseline
No2:Z(1,ties=17,total=20)=.58,p=1.00
No3:Z(1,ties=16,total=20)=1.00,p=.625
No4*:Z(1,ties=10,total=20)=2.89,p=.002
No5*:Z(1,ties=5,total=20)=3.21,p=.001
No6*:Z(1,ties=7,total=20)=3.22,p=.000
No7*:Z(1,ties=14,total=20)=2.26,p=.031
No8*:Z(1,ties=7,total=20)=2.54,p=.014.

*NrCheck.
*Parametric.
GLM NrCheckPreNo1 NrCheckPreNo2 NrCheckPreNo3 NrCheckPreNo4 NrCheckPreNo5
    NrCheckPreNo6 NrCheckPreNo7 NrCheckChaNo1 NrCheckChaNo2 NrCheckChaNo3
    NrCheckChaNo4 NrCheckChaNo5 NrCheckChaNo6 NrCheckChaNo7
  /WSFACTOR=condi 2 Polynomial loc 7 Polynomial 
  /METHOD=SSTYPE(3)
  /PLOT=PROFILE(loc*condi)
  /EMMEANS=TABLES(condi) 
  /EMMEANS=TABLES(loc) 
  /EMMEANS=TABLES(condi*loc) 
  /PRINT=DESCRIPTIVE ETASQ OPOWER PARAMETER 
  /CRITERIA=ALPHA(.05)
  /WSDESIGN=condi loc condi*loc.
**main_block baseline
condi:F(1,19)=3.28,MSE=.35,p=.086,eta=.15,power=.41
loc:F(6.114)=1.30,MSE=.18,p=.282,eta=.06,power=.35, epsilon=.55
condiXNo:F(6,114)=1.25,MSE=.16,p=.301,eta=.06,power=.34, epsilon=.57.
T-TEST PAIRS=NrCheckPreNo1 NrCheckPreNo2 NrCheckPreNo3
    NrCheckPreNo4 NrCheckPreNo5 NrCheckPreNo6 NrCheckPreNo7  WITH
    NrCheckChaNo1 NrCheckChaNo2 NrCheckChaNo3 NrCheckChaNo4 
    NrCheckChaNo5 NrCheckChaNo6 NrCheckChaNo7 (PAIRED)
  /CRITERIA=CI(.9500)
  /MISSING=ANALYSIS.
**main_block baseline
No1:t(19)=1.68,SE=.12,p=.109
No2:t(19)=1.42,SE=.12,p=.172
No3:t(19)=1.02,SE=.11,p=.320
No4:t(19)=.58,SE=.10,p=.570
No5:t(19)=.38,SE=.05,p=.711
No6*:t(19)=2.33,SE=.02,p=.031->less during change
No7*:t(19)=2.18,SE=.02,p=.042->less during change
*Non-Parametric.
NPAR TESTS
  /FRIEDMAN=NrCheckDiffNo1 NrCheckDiffNo2 NrCheckDiffNo3 NrCheckDiffNo4
    NrCheckDiffNo5 NrCheckDiffNo6 NrCheckDiffNo7 
  /STATISTICS DESCRIPTIVES QUARTILES
  /MISSING LISTWISE
  /METHOD=EXACT TIMER(1).
**main_block baseline
Chi2(1,6;n=20)=3.50,p=.744(asymptotisch).
NPAR TESTS
  /WILCOXON=NrCheckPreNo1 NrCheckPreNo2 NrCheckPreNo3 NrCheckPreNo4
    NrCheckPreNo5 NrCheckPreNo6 NrCheckPreNo7 WITH
    NrCheckChaNo1 NrCheckChaNo2 NrCheckChaNo3 NrCheckChaNo4
    NrCheckChaNo5 NrCheckChaNo6 NrCheckChaNo7 (PAIRED)
  /STATISTICS DESCRIPTIVES QUARTILES
  /MISSING ANALYSIS
  /METHOD=EXACT TIMER(1).
**main_block baseline
No1:Z(1,ties=13,total=20)=1.36,p=.234
No2:Z(1,ties=12,total=20)=.43,p=.648
No3:Z(1,ties=12,total=20)=.43,p=.727
No4:Z(1,ties=14,total=20)=.95,p=.438
No5:Z(1,ties=14,total=20)=.97,p=.406
No6(*):Z(1,ties=15,total=20)=2.07,p=.063
No7(*):Z(1,ties=15,total=20)=2.06,p=.063


*******************************************************CA by Trial effects.
*load.
GET DATA  /TYPE=TXT
  /FILE="E:\Experimente\NCT\NCT_Results\NCT_ExpectationDiscrepancy\ExpectDiscrep_V8\results\SPSStabs\"
   +"CA_block.txt"
  /ENCODING='Locale'
  /DELCASE=LINE
  /DELIMITERS="\t"
  /ARRANGEMENT=DELIMITED
  /FIRSTCASE=2
  /IMPORTCASE=ALL
  /VARIABLES=
  Subject F2.0
  NrSearchPreCA1 F11.7
  NrSearchPreCA2 F11.7
  NrSearchPreCA3 F11.7
  NrSearchPreCA4 F11.7
  NrSearchPreCA5 F11.7
  NrSearchPreCA6 F11.7
  NrSearchPreCA7 F11.7
  NrSearchPreCA8 F11.7
  NrSearchChaCA1 F11.7
  NrSearchChaCA2 F11.7
  NrSearchChaCA3 F11.7
  NrSearchChaCA4 F11.7
  NrSearchChaCA5 F11.7
  NrSearchChaCA6 F11.7
  NrSearchChaCA7 F11.7
  NrSearchChaCA8 F11.7
  NrCheckPreCA1 F11.7
  NrCheckPreCA2 F11.7
  NrCheckPreCA3 F11.7
  NrCheckPreCA4 F11.7
  NrCheckPreCA5 F11.7
  NrCheckPreCA6 F11.7
  NrCheckPreCA7 F11.7
  NrCheckPreCA8 F11.7
  NrCheckChaCA1 F11.7
  NrCheckChaCA2 F11.7
  NrCheckChaCA3 F11.7
  NrCheckChaCA4 F11.7
  NrCheckChaCA5 F11.7
  NrCheckChaCA6 F11.7
  NrCheckChaCA7 F11.7
  NrCheckChaCA8 F11.7
  EpathPreCA1 F11.7
  EpathPreCA2 F11.7
  EpathPreCA3 F11.7
  EpathPreCA4 F11.7
  EpathPreCA5 F11.7
  EpathPreCA6 F11.7
  EpathPreCA7 F11.7
  EpathPreCA8 F11.7
  EpathChaCA1 F11.7
  EpathChaCA2 F11.7
  EpathChaCA3 F11.7
  EpathChaCA4 F11.7
  EpathChaCA5 F11.7
  EpathChaCA6 F11.7
  EpathChaCA7 F11.7
  EpathChaCA8 F11.7
  ECdistPreCA1 F11.7
  ECdistPreCA2 F11.7
  ECdistPreCA3 F11.7
  ECdistPreCA4 F11.7
  ECdistPreCA5 F11.7
  ECdistPreCA6 F11.7
  ECdistPreCA7 F11.7
  ECdistPreCA8 F11.7
  ECdistChaCA1 F11.7
  ECdistChaCA2 F11.7
  ECdistChaCA3 F11.7
  ECdistChaCA4 F11.7
  ECdistChaCA5 F11.7
  ECdistChaCA6 F11.7
  ECdistChaCA7 F11.7
  ECdistChaCA8 F11.7
  timePreCA1 F11.7
  timePreCA2 F11.7
  timePreCA3 F11.7
  timePreCA4 F11.7
  timePreCA5 F11.7
  timePreCA6 F11.7
  timePreCA7 F11.7
  timePreCA8 F11.7
  timeChaCA1 F11.7
  timeChaCA2 F11.7
  timeChaCA3 F11.7
  timeChaCA4 F11.7
  timeChaCA5 F11.7
  timeChaCA6 F11.7
  timeChaCA7 F11.7
  timeChaCA8 F11.7
  CpathPreCA1 F11.7
  CpathPreCA2 F11.7
  CpathPreCA3 F11.7
  CpathPreCA4 F11.7
  CpathPreCA5 F11.7
  CpathPreCA6 F11.7
  CpathPreCA7 F11.7
  CpathPreCA8 F11.7
  CpathChaCA1 F11.7
  CpathChaCA2 F11.7
  CpathChaCA3 F11.7
  CpathChaCA4 F11.7
  CpathChaCA5 F11.7
  CpathChaCA6 F11.7
  CpathChaCA7 F11.7
  CpathChaCA8 F11.7
  NrErrPreCA1 F11.7
  NrErrPreCA2 F11.7
  NrErrPreCA3 F11.7
  NrErrPreCA4 F11.7
  NrErrPreCA5 F11.7
  NrErrPreCA6 F11.7
  NrErrPreCA7 F11.7
  NrErrPreCA8 F11.7
  NrErrChaCA1 F11.7
  NrErrChaCA2 F11.7
  NrErrChaCA3 F11.7
  NrErrChaCA4 F11.7
  NrErrChaCA5 F11.7
  NrErrChaCA6 F11.7
  NrErrChaCA7 F11.7
  NrErrChaCA8 F11.7
  ErrDistPreCA1 F11.7
  ErrDistPreCA2 F11.7
  ErrDistPreCA3 F11.7
  ErrDistPreCA4 F11.7
  ErrDistPreCA5 F11.7
  ErrDistPreCA6 F11.7
  ErrDistPreCA7 F11.7
  ErrDistPreCA8 F11.7
  ErrDistChaCA1 F11.7
  ErrDistChaCA2 F11.7
  ErrDistChaCA3 F11.7
  ErrDistChaCA4 F11.7
  ErrDistChaCA5 F11.7
  ErrDistChaCA6 F11.7
  ErrDistChaCA7 F11.7
  ErrDistChaCA8 F11.7
  SubjectB F11.7
  NrSearchDiffCA1 F11.7
  NrSearchDiffCA2 F11.7
  NrSearchDiffCA3 F11.7
  NrSearchDiffCA4 F11.7
  NrSearchDiffCA5 F11.7
  NrSearchDiffCA6 F11.7
  NrSearchDiffCA7 F11.7
  NrSearchDiffCA8 F11.7
  NrCheckDiffCA1 F11.7
  NrCheckDiffCA2 F11.7
  NrCheckDiffCA3 F11.7
  NrCheckDiffCA4 F11.7
  NrCheckDiffCA5 F11.7
  NrCheckDiffCA6 F11.7
  NrCheckDiffCA7 F11.7
  NrCheckDiffCA8 F11.7
  EpathDiffCA1 F11.7
  EpathDiffCA2 F11.7
  EpathDiffCA3 F11.7
  EpathDiffCA4 F11.7
  EpathDiffCA5 F11.7
  EpathDiffCA6 F11.7
  EpathDiffCA7 F11.7
  EpathDiffCA8 F11.7
  ECdistDiffCA1 F11.7
  ECdistDiffCA2 F11.7
  ECdistDiffCA3 F11.7
  ECdistDiffCA4 F11.7
  ECdistDiffCA5 F11.7
  ECdistDiffCA6 F11.7
  ECdistDiffCA7 F11.7
  ECdistDiffCA8 F11.7
  timeDiffCA1 F11.7
  timeDiffCA2 F11.7
  timeDiffCA3 F11.7
  timeDiffCA4 F11.7
  timeDiffCA5 F11.7
  timeDiffCA6 F11.7
  timeDiffCA7 F11.7
  timeDiffCA8 F11.7
  CpathDiffCA1 F11.7
  CpathDiffCA2 F11.7
  CpathDiffCA3 F11.7
  CpathDiffCA4 F11.7
  CpathDiffCA5 F11.7
  CpathDiffCA6 F11.7
  CpathDiffCA7 F11.7
  CpathDiffCA8 F11.7
  NrErrDiffCA1 F11.7
  NrErrDiffCA2 F11.7
  NrErrDiffCA3 F11.7
  NrErrDiffCA4 F11.7
  NrErrDiffCA5 F11.7
  NrErrDiffCA6 F11.7
  NrErrDiffCA7 F11.7
  NrErrDiffCA8 F11.7
  ErrDistDiffCA1 F11.7
  ErrDistDiffCA2 F11.7
  ErrDistDiffCA3 F11.7
  ErrDistDiffCA4 F11.7
  ErrDistDiffCA5 F11.7
  ErrDistDiffCA6 F11.7
  ErrDistDiffCA7 F11.7
  ErrDistDiffCA8 F11.7.
CACHE.
EXECUTE.
DATASET NAME CA WINDOW=FRONT.

*Test for normal distribution.
NPAR TESTS
  /K-S(NORMAL)=
    timePreCA1 timePreCA2 timePreCA3 timePreCA4
    timePreCA5 timePreCA6 timePreCA7 timePreCA8
    timeChaCA1 timeChaCA2 timeChaCA3 timeChaCA4
    timeChaCA5 timeChaCA6 timeChaCA7 timeChaCA8
    NrErrPreCA1 NrErrPreCA2 NrErrPreCA3 NrErrPreCA4
    NrErrPreCA5 NrErrPreCA6 NrErrPreCA7 NrErrPreCA8
    NrErrChaCA1 NrErrChaCA2 NrErrChaCA3 NrErrChaCA4
    NrErrChaCA5 NrErrChaCA6 NrErrChaCA7 NrErrChaCA8
    ErrDistPreCA1 ErrDistPreCA2 ErrDistPreCA3 ErrDistPreCA4
    ErrDistPreCA5 ErrDistPreCA6 ErrDistPreCA7 ErrDistPreCA8
    ErrDistChaCA1 ErrDistChaCA2 ErrDistChaCA3 ErrDistChaCA4
    ErrDistChaCA5 ErrDistChaCA6 ErrDistChaCA7 ErrDistChaCA8
    NrSearchPreCA1 NrSearchPreCA2 NrSearchPreCA3 NrSearchPreCA4
    NrSearchPreCA5 NrSearchPreCA6 NrSearchPreCA7 NrSearchPreCA8
    NrSearchChaCA1 NrSearchChaCA2 NrSearchChaCA3 NrSearchChaCA4
    NrSearchChaCA5 NrSearchChaCA6 NrSearchChaCA7 NrSearchChaCA8
    NrCheckPreCA1 NrCheckPreCA2 NrCheckPreCA3 NrCheckPreCA4
    NrCheckPreCA5 NrCheckPreCA6 NrCheckPreCA7 NrCheckPreCA8
    NrCheckChaCA1 NrCheckChaCA2 NrCheckChaCA3 NrCheckChaCA4
    NrCheckChaCA5 NrCheckChaCA6 NrCheckChaCA7 NrCheckChaCA8
    CpathPreCA1 CpathPreCA2 CpathPreCA3 CpathPreCA4
    CpathPreCA5 CpathPreCA6 CpathPreCA7 CpathPreCA8
    CpathChaCA1 CpathChaCA2 CpathChaCA3 CpathChaCA4
    CpathChaCA5 CpathChaCA6 CpathChaCA7 CpathChaCA8
    EpathPreCA1 EpathPreCA2 EpathPreCA3 EpathPreCA4
    EpathPreCA5 EpathPreCA6 EpathPreCA7 EpathPreCA8
    EpathChaCA1 EpathChaCA2 EpathChaCA3 EpathChaCA4
    EpathChaCA5 EpathChaCA6 EpathChaCA7 EpathChaCA8
    ECdistPreCA1 ECdistPreCA2 ECdistPreCA3 ECdistPreCA4
    ECdistPreCA5 ECdistPreCA6 ECdistPreCA7 ECdistPreCA8
    ECdistChaCA1 ECdistChaCA2 ECdistChaCA3 ECdistChaCA4
    ECdistChaCA5 ECdistChaCA6 ECdistChaCA7 ECdistChaCA8
  /STATISTICS DESCRIPTIVES QUARTILES
  /MISSING ANALYSIS
  /METHOD=EXACT TIMER(1).
**main_block baseline
NotNormal:
timeChaCA24,
NrErrPreCA1234567, NrErrChaCA2346, ErrDistPreCA8,
NrSearchPreCA23567, NrSearchChaCA12567, all NrCheck
CpathChaCA2, EpathChaCA8.

*time.
*parametric.
GLM timePreCA1 timePreCA2 timePreCA3 timePreCA4
    timePreCA5 timePreCA6 timePreCA7 timePreCA8 
    timeChaCA1 timeChaCA2 timeChaCA3 timeChaCA4
    timeChaCA5 timeChaCA6 timeChaCA7 timeChaCA8
  /WSFACTOR=Condi 2 Polynomial CA 8 Polynomial 
  /METHOD=SSTYPE(3)
  /PLOT=PROFILE(CA*Condi)
  /EMMEANS=TABLES(CA) COMPARE ADJ(LSD)
  /EMMEANS=TABLES(Condi) COMPARE ADJ(LSD)
  /EMMEANS=TABLES(CA*Condi) 
  /PRINT=DESCRIPTIVE ETASQ OPOWER PARAMETER 
  /CRITERIA=ALPHA(.05)
  /WSDESIGN=Condi CA Condi*CA.
**main_block baseline
Condi*:F(1,19)=15.12,MSE=5012783.16,p=.001,eta=.44,power=.96
CA*:F(7,133)=10.62,MSE=5888568.46,p=.000,eta=.36,power=.99, epsilon=.34
CAxCondi*:F(7,133)=10.97,MSE=5673759.46,p=.000,eta=.37,power=.99, epsilon=.35.
T-TEST PAIRS=timePreCA1 timePreCA2 timePreCA3 timePreCA4
    timePreCA5 timePreCA6 timePreCA7 timePreCA8 WITH
    timeChaCA1 timeChaCA2 timeChaCA3 timeChaCA4
    timeChaCA5 timeChaCA6 timeChaCA7 timeChaCA8 (PAIRED)
  /CRITERIA=CI(.9500)
  /MISSING=ANALYSIS.
**main_block baseline
CA1:t(19)=1.90,SE=42.60,p=.073
CA2:t(19)=.22,SE=159.93,p=.830
CA3*:t(19)=6.72,SE=189.88,p=.000
CA4:t(19)=1.59,SE=276.75,p=.129
CA5:t(19)=1.78,SE=38.79,p=.091
CA6*:t(19)=2.57,SE=78.32,p=.019
CA7:t(19)=.99,SE=85.94,p=.335
CA8:t(19)=.35,SE=61.00,p=.730.

*NrErr.
*parametric.
GLM NrErrPreCA1 NrErrPreCA2 NrErrPreCA3 NrErrPreCA4
    NrErrPreCA5 NrErrPreCA6 NrErrPreCA7 NrErrPreCA8 
    NrErrChaCA1 NrErrChaCA2 NrErrChaCA3 NrErrChaCA4
    NrErrChaCA5 NrErrChaCA6 NrErrChaCA7 NrErrChaCA8
  /WSFACTOR=Condi 2 Polynomial CA 8 Polynomial 
  /METHOD=SSTYPE(3)
  /PLOT=PROFILE(CA*Condi)
  /EMMEANS=TABLES(CA) COMPARE ADJ(LSD)
  /EMMEANS=TABLES(Condi) COMPARE ADJ(LSD)
  /EMMEANS=TABLES(CA*Condi) 
  /PRINT=DESCRIPTIVE ETASQ OPOWER PARAMETER 
  /CRITERIA=ALPHA(.05)
  /WSDESIGN=Condi CA Condi*CA.
**main_block baseline
Condi:F(1,19)=.50,MSE=.17,p=.490,eta=.03,power=.10
CA:F(7,133)=1.28,MSE=1.92,p=.286,eta=.06,power=.23,epsilon=.22
CAxCondi:F(7,133)=1.02,MSE=1.59,p=.355,eta=.05,power=.19,epsilon=.22.
T-TEST PAIRS=NrErrPreCA1 NrErrPreCA2 NrErrPreCA3 NrErrPreCA4
    NrErrPreCA5 NrErrPreCA6 NrErrPreCA7 NrErrPreCA8 WITH
    NrErrChaCA1 NrErrChaCA2 NrErrChaCA3 NrErrChaCA4
    NrErrChaCA5 NrErrChaCA6 NrErrChaCA7 NrErrChaCA8 (PAIRED)
  /CRITERIA=CI(.9500)
  /MISSING=ANALYSIS.
**main_block baseline
CA1*:t(19)=3.10,SE=.03,p=.006 -> less in cha
CA2:t(19)=.29,SE=.21,p=.776
CA3*:t(19)=1.53,SE=.15,p=.142
CA4:t(19)=.93,SE=.45,p=.363
CA5*:t(19)=2.67,SE=.01,p=.015 -> less in cha
CA6*:t(19)=.34,SE=.06,p=.741
CA7*:t(19)=2.54,SE=.03,p=.020 -> less in cha
CA8*:t(19)=3.22,SE=.04,p=.005. -> less in cha

*ErrDist.
*parametric.
GLM ErrDistPreCA1 ErrDistPreCA2 ErrDistPreCA3 ErrDistPreCA4
    ErrDistPreCA5 ErrDistPreCA6 ErrDistPreCA7 ErrDistPreCA8 
    ErrDistChaCA1 ErrDistChaCA2 ErrDistChaCA3 ErrDistChaCA4
    ErrDistChaCA5 ErrDistChaCA6 ErrDistChaCA7 ErrDistChaCA8
  /WSFACTOR=Condi 2 Polynomial CA 8 Polynomial 
  /METHOD=SSTYPE(3)
  /PLOT=PROFILE(CA*Condi)
  /EMMEANS=TABLES(CA) COMPARE ADJ(LSD)
  /EMMEANS=TABLES(Condi) COMPARE ADJ(LSD)
  /EMMEANS=TABLES(CA*Condi) 
  /PRINT=DESCRIPTIVE ETASQ OPOWER PARAMETER 
  /CRITERIA=ALPHA(.05)
  /WSDESIGN=Condi CA Condi*CA.
**main_block baseline
*not enough valid cases!.
T-TEST PAIRS=ErrDistPreCA1 ErrDistPreCA2 ErrDistPreCA3 ErrDistPreCA4
    ErrDistPreCA5 ErrDistPreCA6 ErrDistPreCA7 ErrDistPreCA8 WITH
    ErrDistChaCA1 ErrDistChaCA2 ErrDistChaCA3 ErrDistChaCA4
    ErrDistChaCA5 ErrDistChaCA6 ErrDistChaCA7 ErrDistChaCA8 (PAIRED)
  /CRITERIA=CI(.9500)
  /MISSING=ANALYSIS.
**main_block baseline
CA1:not enough valid cases!
CA2:not enough valid cases!
CA3*:t(3)=4.94,SE=7.03,p=.016
CA4:not enough valid cases!
CA5:not enough valid cases!
CA6:not enough valid cases!
CA7:not enough valid cases!
CA8:not enough valid cases!.

*Cpath.
*parametric.
GLM CpathPreCA1 CpathPreCA2 CpathPreCA3 CpathPreCA4
    CpathPreCA5 CpathPreCA6 CpathPreCA7 CpathPreCA8 
    CpathChaCA1 CpathChaCA2 CpathChaCA3 CpathChaCA4
    CpathChaCA5 CpathChaCA6 CpathChaCA7 CpathChaCA8
  /WSFACTOR=Condi 2 Polynomial CA 8 Polynomial 
  /METHOD=SSTYPE(3)
  /PLOT=PROFILE(CA*Condi)
  /EMMEANS=TABLES(CA) COMPARE ADJ(LSD)
  /EMMEANS=TABLES(Condi) COMPARE ADJ(LSD)
  /EMMEANS=TABLES(CA*Condi) 
  /PRINT=DESCRIPTIVE ETASQ OPOWER PARAMETER 
  /CRITERIA=ALPHA(.05)
  /WSDESIGN=Condi CA Condi*CA.
**main_block baseline
Condi*:F(1,19)=21.94,MSE=3527.31,p=.000,eta=.54,power=.99
CA*:F(7,133)=25.41,MSE=4316.72,p=.000,eta=.57,power=1.00, epsilon=.504
CAxCondi*:F(7,133)=11.94,MSE=3922.22,p=.000,eta=.39,power=1.00, epsilon=.407.
T-TEST PAIRS=CpathPreCA1 CpathPreCA2 CpathPreCA3 CpathPreCA4
    CpathPreCA5 CpathPreCA6 CpathPreCA7 CpathPreCA8 WITH
    CpathChaCA1 CpathChaCA2 CpathChaCA3 CpathChaCA4
    CpathChaCA5 CpathChaCA6 CpathChaCA7 CpathChaCA8 (PAIRED)
  /CRITERIA=CI(.9500)
  /MISSING=ANALYSIS.
**main_block baseline
CA1:t(19)=1.81,SE=1.89,p=.087
CA2:t(19)=.43,SE=3.18,p=.670
CA3*:t(19)=10.20,SE=3.52,p=.000
CA4:t(19)=1.71,SE=5.63,p=.103
CA5:t(19)=1.09,SE=1.27,p=.288
CA6:t(19)=1.99,SE=3.92,p=.061
CA7:t(19)=.55,SE=5.86,p=.592
CA8:t(19)=.03,SE=.72,p=.978.

*NrSearch.
*parametric.
GLM NrSearchPreCA1 NrSearchPreCA2 NrSearchPreCA3 NrSearchPreCA4
    NrSearchPreCA5 NrSearchPreCA6 NrSearchPreCA7 
    NrSearchChaCA1 NrSearchChaCA2 NrSearchChaCA3 NrSearchChaCA4
    NrSearchChaCA5 NrSearchChaCA6 NrSearchChaCA7
  /WSFACTOR=Condi 2 Polynomial CA 7 Polynomial 
  /METHOD=SSTYPE(3)
  /PLOT=PROFILE(CA*Condi)
  /EMMEANS=TABLES(CA) COMPARE ADJ(LSD)
  /EMMEANS=TABLES(Condi) COMPARE ADJ(LSD)
  /EMMEANS=TABLES(CA*Condi) 
  /PRINT=DESCRIPTIVE ETASQ OPOWER PARAMETER 
  /CRITERIA=ALPHA(.05)
  /WSDESIGN=Condi CA Condi*CA.
**main_block baseline
Condi*:F(1,19)=53.43,MSE=28.16,p=.000,eta=.74,power=1.00
CA*:F(6,114)=20.85,MSE=25.08,p=.000,eta=.52,power=1.00, epsilon=.416
CAxCondi*:F(6,114)=19.74,MSE=19.90,p=.000,eta=.51,power=1.00, epsilon=.483.
T-TEST PAIRS=NrSearchPreCA1 NrSearchPreCA2 NrSearchPreCA3
    NrSearchPreCA4 NrSearchPreCA5 NrSearchPreCA6 NrSearchPreCA7 WITH
    NrSearchChaCA1 NrSearchChaCA2 NrSearchChaCA3 NrSearchChaCA4
    NrSearchChaCA5 NrSearchChaCA6 NrSearchChaCA7 (PAIRED)
  /CRITERIA=CI(.9500)
  /MISSING=ANALYSIS.
**main_block baseline
CA1:t(19)=1.79,SE=.06,p=.089
CA2:t(19)=1.02,SE=.31,p=.319
CA3*:t(19)=7.71,SE=.36,p=.000
CA4*:t(19)=3.51,SE=.24,p=.002
CA5:t(19)=.95,SE=.10,p=.352
CA6(*):t(19)=1.97,SE=.21,p=.064
CA7:t(19)=1.75,SE=.07,p=.097.
*non-parametric.
NPAR TESTS
  /FRIEDMAN=NrSearchDiffCA1 NrSearchDiffCA2 NrSearchDiffCA3
    NrSearchDiffCA4 NrSearchDiffCA5 NrSearchDiffCA6 NrSearchDiffCA7
  /STATISTICS DESCRIPTIVES QUARTILES
  /MISSING LISTWISE
  /METHOD=EXACT TIMER(1).
**main_block baseline
Chi2(1,6,n=20)=58.76,p=.000(asymptotisch).
NPAR TESTS
  /WILCOXON= NrSearchPreCA1 NrSearchPreCA2 NrSearchPreCA3
    NrSearchPreCA4 NrSearchPreCA5 NrSearchPreCA6 NrSearchPreCA7 WITH
    NrSearchChaCA1 NrSearchChaCA2 NrSearchChaCA3 NrSearchChaCA4
    NrSearchChaCA5 NrSearchChaCA6 NrSearchChaCA7 (PAIRED)
  /STATISTICS DESCRIPTIVES QUARTILES
  /MISSING ANALYSIS
  /METHOD=EXACT TIMER(1).
**main_block baseline
CA1*:Z(1,ties=5,total=20)=2.17,p=.027
CA2:Z(1,ties=5,total=20)=.03,p=.986
CA3*:Z(1,ties=1,total=20)=3.83,p=.000
CA4*:Z(1,ties=2,total=20)=3.08,p=.001
CA5:Z(1,ties=13,total=20)=.17,p=.922
CA6:Z(1,ties=8,total=20)=1.34,p=.194
CA7:Z(1,ties=16,total=20)=1.51,p=.250.

*Epath.
*parametric.
GLM EpathPreCA1 EpathPreCA2 EpathPreCA3 EpathPreCA4
    EpathPreCA5 EpathPreCA6 EpathPreCA7 EpathPreCA8 
    EpathChaCA1 EpathChaCA2 EpathChaCA3 EpathChaCA4
    EpathChaCA5 EpathChaCA6 EpathChaCA7 EpathChaCA8
  /WSFACTOR=Condi 2 Polynomial CA 8 Polynomial 
  /METHOD=SSTYPE(3)
  /PLOT=PROFILE(CA*Condi)
  /EMMEANS=TABLES(CA) COMPARE ADJ(LSD)
  /EMMEANS=TABLES(Condi) COMPARE ADJ(LSD)
  /EMMEANS=TABLES(CA*Condi) 
  /PRINT=DESCRIPTIVE ETASQ OPOWER PARAMETER 
  /CRITERIA=ALPHA(.05)
  /WSDESIGN=Condi CA Condi*CA.
**main_block baseline
Condi*:F(1,19)=26.85,MSE=10217.50,p=.000,eta=.59,power=1.00
CA*:F(7,133)=19.64,MSE=6531.89,p=.000,eta=.51,power=1.00, epsilon=.556
CAxCondi*:F(7,133)=19.87,MSE=7838.74,p=.000,eta=.51,power=1.00, epsilon=.494.
T-TEST PAIRS=EpathPreCA1 EpathPreCA2 EpathPreCA3 EpathPreCA4
    EpathPreCA5 EpathPreCA6 EpathPreCA7 EpathPreCA8 WITH
    EpathChaCA1 EpathChaCA2 EpathChaCA3 EpathChaCA4
    EpathChaCA5 EpathChaCA6 EpathChaCA7 EpathChaCA8 (PAIRED)
  /CRITERIA=CI(.9500)
  /MISSING=ANALYSIS.
**main_trial baseline
CA1:t(19)=1.14,SE=1.94,p=.268
CA2:t(19)=.06,SE=4.12,p=.953
CA3*:t(19)=10.32,SE=5.57,p=.000
CA4*:t(19)=2.25,SE=6.20,p=.037
CA5:t(19)=1.36,SE=3.51,p=.191
CA6*:t(19)=3.38,SE=3.93,p=.003
CA7:t(19)=.80,SE=6.33,p=.432
CA8:t(19)=.42,SE=4.06,p=.681.

*ECdist.
*parametric.
GLM ECdistPreCA1 ECdistPreCA2 ECdistPreCA3 ECdistPreCA4
    ECdistPreCA5 ECdistPreCA6 ECdistPreCA7 ECdistPreCA8 
    ECdistChaCA1 ECdistChaCA2 ECdistChaCA3 ECdistChaCA4
    ECdistChaCA5 ECdistChaCA6 ECdistChaCA7 ECdistChaCA8
  /WSFACTOR=Condi 2 Polynomial CA 8 Polynomial 
  /METHOD=SSTYPE(3)
  /PLOT=PROFILE(CA*Condi)
  /EMMEANS=TABLES(CA) COMPARE ADJ(LSD)
  /EMMEANS=TABLES(Condi) COMPARE ADJ(LSD)
  /EMMEANS=TABLES(CA*Condi) 
  /PRINT=DESCRIPTIVE ETASQ OPOWER PARAMETER 
  /CRITERIA=ALPHA(.05)
  /WSDESIGN=Condi CA Condi*CA.
**main_block baseline
Condi*:F(1,19)=51.73,MSE=147.65,p=.000,eta=.73,power=1.00
CA*:F(7,133)=17.00,MSE=29.23,p=.000,eta=.47,power=1.00
CAxCondi*:F(7,133)=3.35,MSE=23.65,p=.025,eta=.15,power=.73, epsilon=.428..
T-TEST PAIRS=ECdistPreCA1 ECdistPreCA2 ECdistPreCA3 ECdistPreCA4
    ECdistPreCA5 ECdistPreCA6 ECdistPreCA7 ECdistPreCA8 WITH
    ECdistChaCA1 ECdistChaCA2 ECdistChaCA3 ECdistChaCA4
    ECdistChaCA5 ECdistChaCA6 ECdistChaCA7 ECdistChaCA8 (PAIRED)
  /CRITERIA=CI(.9500)
  /MISSING=ANALYSIS.
**main_block baseline
CA1:t(19)=.15,SE=.40,p=.883
CA2:t(19)=.90,SE=.31,p=.378
CA3*:t(19)=7.90,SE=.34,p=.000
CA4*:t(19)=3.67,SE=.65,p=.002
CA5*:t(19)=2.32,SE=.62,p=.032
CA6*:t(19)=4.81,SE=.48,p=.000
CA7:t(19)=.85,SE=.89,p=.405
CA8*:t(19)=2.21,SE=.43,p=.040.

*NrCheck.
*parametric.
GLM NrCheckPreCA2 NrCheckPreCA3 NrCheckPreCA4
    NrCheckPreCA5 NrCheckPreCA6 NrCheckPreCA7 NrCheckPreCA8
    NrCheckChaCA2 NrCheckChaCA3 NrCheckChaCA4
    NrCheckChaCA5 NrCheckChaCA6 NrCheckChaCA7 NrCheckChaCA8
  /WSFACTOR=Condi 2 Polynomial CA 7 Polynomial 
  /METHOD=SSTYPE(3)
  /PLOT=PROFILE(CA*Condi)
  /EMMEANS=TABLES(CA) COMPARE ADJ(LSD)
  /EMMEANS=TABLES(Condi) COMPARE ADJ(LSD)
  /EMMEANS=TABLES(CA*Condi) 
  /PRINT=DESCRIPTIVE ETASQ OPOWER PARAMETER 
  /CRITERIA=ALPHA(.05)
  /WSDESIGN=Condi CA Condi*CA.
**main_block baseline
Condi:F(1,19)=3.28,MSE=.35,p=.086,eta=.15,power=.41
CA:F(6,114)=.61,MSE=.16,p=.623,eta=.03,power=.17, epsilon=531
CAxCondi:F(6,114)=.32,MSE=.09,p=.818,eta=.02,power=.11, epsilon=.519.
T-TEST PAIRS=NrCheckPreCA2 NrCheckPreCA3 NrCheckPreCA4
    NrCheckPreCA5 NrCheckPreCA6 NrCheckPreCA7 NrCheckPreCA8 WITH
    NrCheckChaCA2 NrCheckChaCA3 NrCheckChaCA4 NrCheckChaCA5
    NrCheckChaCA6 NrCheckChaCA7 NrCheckChaCA8 (PAIRED)
  /CRITERIA=CI(.9500)
  /MISSING=ANALYSIS.
**main_block baseline
CA2:t(19)=.30,SE=.05,p=.772
CA3:t(19)=1.09,SE=.15,p=.290
CA4:t(19)=.53,SE=.05,p=.605
CA5:t(19)=1.71,SE=.01,p=.104
CA6:t(19)=.79,SE=.10,p=.440
CA7:t(19)=.89,SE=.11,p=.387
CA8:t(19)=.63,SE=.21,p=.539
*non-parametric.
NPAR TESTS
  /FRIEDMAN=NrCheckDiffCA2 NrCheckDiffCA3 NrCheckDiffCA4
    NrCheckDiffCA5 NrCheckDiffCA6 NrCheckDiffCA7 NrCheckDiffCA8
  /STATISTICS DESCRIPTIVES QUARTILES
  /MISSING LISTWISE
  /METHOD=EXACT TIMER(1).
**main_block baseline
Chi2(1,6,n=20)=.57,p=.997(asymptotisch).
NPAR TESTS
  /WILCOXON= NrCheckPreCA2 NrCheckPreCA3 NrCheckPreCA4
    NrCheckPreCA5 NrCheckPreCA6 NrCheckPreCA7 NrCheckPreCA8 WITH
    NrCheckChaCA2 NrCheckChaCA3 NrCheckChaCA4 NrCheckChaCA5
    NrCheckChaCA6 NrCheckChaCA7 NrCheckChaCA8 (PAIRED)
  /STATISTICS DESCRIPTIVES QUARTILES
  /MISSING ANALYSIS
  /METHOD=EXACT TIMER(1).
**main_block baseline
CA2:Z(1,ties=16,total=20)=.37,p=.750
CA3:Z(1,ties=14,total=20)=.11,p=1.00
CA4:Z(1,ties=16,total=20)=.37,p=.750
CA5:Z(1,ties=17,total=20)=1.63,p=.250
CA6:Z(1,ties=15,total=20)=.71,p=.750
CA7:Z(1,ties=13,total=20)=.17,p=.953
CA8:Z(1,ties=14,total=20)=.95,p=.438





